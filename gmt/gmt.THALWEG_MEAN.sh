#!/bin/bash
###########################################################################
gmtset FONT_ANNOT_PRIMARY=10p,Courier,black FONT_ANNOT_SECONDARY=10p,Courier,black FONT_LABEL=10p,Courier,black
gmtset FORMAT_DATE_MAP "o" FORMAT_TIME_PRIMARY_MAP Abbreviated
gmtset MAP_FRAME_PEN thinnest,black MAP_FRAME_TYPE plain MAP_FRAME_WIDTH 2p
gmtset MAP_TICK_LENGTH_PRIMARY 2p/0.5p MAP_TICK_LENGTH_SECONDARY 5p/1.75p
gmtset COLOR_BACKGROUND white COLOR_FOREGROUND black
PAPER="20ix10i"
###########################################################################
VAR=SAL
EXPNAME=BAS; EXPNO=02; EXP=${EXPNAME}${EXPNO}
YR=2009; DAY=12; IYR=${YR}-01-01; FYR=${YR}-12-31; 
DATADIR=CALCTMEAN
FILENAME=${DATADIR}/${DATADIR}_${EXP}_${YR}_MNTH${DAY}
if [[ "${VAR}" = TEM ]]; then
       	COL=9; YMIN=5; YMAX=30; YINC=0.1; UNITY="@+0@+C"; CINT=5
	VARNAME="TEMPERATURE";
elif [[ "${VAR}" = SAL ]]; then
       	COL=10; YMIN=16; YMAX=40; YINC=0.1; UNITY=psu; CINT=4
	VARNAME="SALINITY";
fi
###########################################################################
fig=${FILENAME}_${VAR}; ps=${fig}.ps
cptfile=${DATADIR}/CPT_${EXP}.cpt
outpng=${DATADIR}/out.png
REGIONT=1/295/-150/0; PROJT="X6i/1.8i"; BASE1T=50:DISTANCE::,km:/25:DEPTH:WSen; 
makecpt -Csst -T${YMIN}/${YMAX}/${YINC} -N > ${cptfile}
psbasemap -R${REGIONT} -J${PROJT} -B${BASE1T} --PS_MEDIA=${PAPER} -K > $ps
###########################################################################
awk '{print $3,-$8,$'${COL}'}' ${FILENAME}.asc | pscontour -R -J -B -K -O -C${cptfile} -I -V3 >> $ps
echo "1.97 0 1" > coords.dat
echo "1.97 -150 1" >> coords.dat
psxy coords.dat -R -J -B -W0.5,black -K -O >> ${ps}
echo "10 -115 Dardanelles" | pstext -R -J -F+a20+jTL+f11p,Arial,black -O -K -N  >> $ps
echo "70.18 0 1" > coords.dat
echo "70.18 -150 1" >> coords.dat
psxy coords.dat -R -J -B -W0.5,black -K -O >> ${ps}
echo "243.4 0 1" > coords.dat
echo "243.4 -150 1" >> coords.dat
psxy coords.dat -R -J -B -W0.5,black -K -O >> ${ps}
echo "244.1 -140 Bosphorus" | pstext -R -J -F+a60+jTL+f11p,Arial,black -O -K -N  >> $ps
echo "267.9 0 1" > coords.dat
echo "267.9 -150 1" >> coords.dat
psxy coords.dat -R -J -B -W0.5,black -K -O >> ${ps}
rm coords.dat
psscale -D0.7i/0.05i/1.2i/0.1h -Aal -C${cptfile} -B${CINT}/:${UNITY}: -O -K -V3 >> $ps
echo "50 -115 " | pstext -R -J -F+jTL+f15p,Arial,black -O -N  >> $ps
ps2raster -Tg -V3 -P -F${outpng} $ps 
convert -trim -alpha off -antialias -scale 80% ${outpng} ${fig}.png
rm ${ps} ${cptfile} ${outpng}
