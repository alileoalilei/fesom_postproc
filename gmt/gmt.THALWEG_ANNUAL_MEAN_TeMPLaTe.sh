#!/bin/bash
###########################################################################
gmtset FONT_ANNOT_PRIMARY=9p,Courier-Bold,black FONT_ANNOT_SECONDARY=9p,Courier-Bold,black FONT_LABEL=9p,Courier-Bold,black
gmtset FORMAT_DATE_MAP "o" FORMAT_TIME_PRIMARY_MAP Abbreviated
gmtset MAP_FRAME_PEN thinnest,black MAP_FRAME_TYPE plain MAP_FRAME_WIDTH 2p
gmtset MAP_TICK_LENGTH_PRIMARY 2p/0.5p MAP_TICK_LENGTH_SECONDARY 5p/1.75p
PAPER="20ix10i"
###########################################################################
VAR=VARIABLENAME
EXPNAME=EXPDEF; EXPNO=EXPNUM; EXP=${EXPNAME}${EXPNO}
YR=YEAROFNC; DAY=DAYOFNC; IYR=${YR}-01-01; FYR=${YR}-12-31; 
DATE=${YR}
FILENAME=CALCTMEAN_${EXP}_${YR}_ANNUAL
cptfile=CPT_${EXP}.cpt
if [[ "${VAR}" = TEM ]]; then
        gmtset COLOR_BACKGROUND white COLOR_FOREGROUND gray
       	COL=9; YMIN=4; YMAX=20; YINC=0.1; UNITY="@+0@+C"; CINT=4
       	COL=9; UMIN=4; UMAX=20; UINC=4; 
       	COL=9; BMIN=13; BMAX=15; BINC=0.5; 
        makecpt -Csst -T${YMIN}/${YMAX}/${YINC} -N -M > ${cptfile}
	VARNAME="TEMPERATURE";
	OFFSET=0
elif [[ "${VAR}" = SAL ]]; then
        gmtset COLOR_BACKGROUND white COLOR_FOREGROUND black
       	COL=10; YMIN=16; YMAX=40; YINC=0.1; UNITY=psu; CINT=4
       	COL=10; UMIN=16; UMAX=40; UINC=4; 
       	COL=10; BMIN=38; BMAX=40; BINC=0.5; 
        makecpt -Csss -T${YMIN}/${YMAX}/${YINC} -N -M > ${cptfile}
	VARNAME="SALINITY";
	OFFSET=0
elif [[ "${VAR}" = DEN ]]; then
        gmtset COLOR_BACKGROUND white COLOR_FOREGROUND black
       	COL=11; YMIN=11; YMAX=33; YINC=0.1; UNITY=kgm@+-3@+; CINT=11
       	COL=11; UMIN=11; UMAX=33; UINC=5; 
       	COL=11; BMIN=11; BMAX=33; BINC=0.5; 
	OFFSET=1000
        makecpt -Ccequal -T${YMIN}/${YMAX}/${YINC} -I -N -M > ${cptfile}
	VARNAME="DENSITY";
fi
###########################################################################
fig=${FILENAME}_${VAR}; ps=${fig}.ps
outpng=out.png
REGIONT=1/540/-100/0; PROJT="X6i/1.8i"; BASE1T=a100f10:DISTANCE::,km:/25:"DEPTH(m)":WSen; 
psbasemap -R${REGIONT} -J${PROJT} -B${BASE1T} --PS_MEDIA=${PAPER} -K > $ps
###########################################################################
#makecpt -Csss -T${YMIN}/${YMAX}/${YINC} -N -M > ${cptfile}
awk '{print $3,-$8,$'${COL}'-'${OFFSET}'}' ${FILENAME}.asc | pscontour -R -J -B -K -O -C${cptfile} -I >> $ps
# contour here if needed
#makecpt -Csst -T${UMIN}/${UMAX}/${UINC} -N -M > ${cptfile}
#awk '{print $3,-$8,$'${COL}'}' ${FILENAME}.asc | pscontour -A-2+jTR+f4p,Courier-Bold,black -R -J -B -K -O -C${cptfile} -W0.2p >> $ps
#makecpt -Csst -T${BMIN}/${BMAX}/${BINC} -N -M > ${cptfile}
#awk '{print $3,-$8,$'${COL}'}' ${FILENAME}.asc | pscontour -A+jTR+f4p,Courier-Bold, -R -J -B -K -O -C${cptfile} -W0.2p,gray >> $ps
#makecpt -Csst -T${YMIN}/${YMAX}/${YINC} -N -M > ${cptfile}
echo "26.921669 0 1" > coords.dat
echo "26.921669 -75 1" >> coords.dat
psxy coords.dat -R -J -B -W0.5,black -K -O >> ${ps}
echo "100 -60 Dardanelles" | pstext -R -J -F+a20+jTR+f9p,Courier-Bold,black -O -K -N  >> $ps
echo "111.225639 0 1" > coords.dat
echo "111.225639 -75 1" >> coords.dat
psxy coords.dat -R -J -B -W0.5,black -K -O >> ${ps}
echo "448.583858 0 1" > coords.dat
echo "448.583858 -100 1" >> coords.dat
psxy coords.dat -R -J -B -W0.5,black -K -O >> ${ps}
echo "450 -95 Bosphorus" | pstext -R -J -F+a65+jTL+f9,Courier-Bold,black -O -K -N  >> $ps
echo "487.982401 0 1" > coords.dat
echo "487.982401 -100 1" >> coords.dat
psxy coords.dat -R -J -B -W0.5,black -K -O >> ${ps}
rm coords.dat
psscale -D1.0i/0.1i/1.2i/0.075h -Aal -C${cptfile} -B${CINT}/:${UNITY}: -O -K >> $ps
echo "220 -95 ${DATE}" | pstext -R -J -F+jBL+f9p,Courier-Bold,black -O -N  >> $ps
ps2raster -Tg -V3 -P -F${outpng} $ps 
convert -trim -alpha off -antialias -scale 80% ${outpng} ${fig}.png
sh ~/script/put2ftp.sh ${fig}.png  CALCTMEAN Public/aydogdu/${EXP}/.
rm ${ps} ${cptfile} ${outpng}
