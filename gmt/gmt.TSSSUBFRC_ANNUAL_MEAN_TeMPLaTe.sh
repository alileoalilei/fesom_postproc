#!/bin/bash
##########################################################################
############# GMT SCRIPT TO PLOT HORIZONTAL SECTIONS FOR TSS #############
############# MODIFY DATE AND LAYER BY A SUBMIT SCRIPT USING SED #########
##########################################################################
### written by: ali aydogdu ##############################################
### last modified: 2015/01/27 ############################################
##########################################################################
date
##########################################################################
############# SET PARAMETERS FROM SUBMIT SCRIPT ##########################
##########################################################################
LAYER=`echo LAYERNUMBER | awk '{printf("%03d\n",$1)}'`
DAY=`echo DAY2PLOT | awk '{printf("%02d\n",$1)}'`
YEAR=YEAR2PLOT
#DATE=$(echo ${DAY} | awk '{printf "%d\n",$0;}')
#DATE=$(sh ~/script/sh/CALCDATE.sh 1 ${YEAR} ${DATE} | )
DATE=${YEAR}
VAR=VARIABLE2PLOT
EXP=EXPCODE
FILENAME=${VAR}_${EXP}_${YEAR}_ANNUAL_LEV${LAYER};
##########################################################################
############# SET GMT DEFAULTS ###########################################
##########################################################################
gmtset PS_MEDIA=20ix14i IO_NAN_RECORDS=pass FORMAT_GEO_MAP ddd:mm:ssF
gmtset FONT_ANNOT_PRIMARY=12p,Courier-Bold,black FONT_ANNOT_SECONDARY=12p,Courier-Bold,black 
gmtset FONT_LABEL=12p,Courier-Bold,black
gmtset IO_NAN_RECORDS=pass FORMAT_GEO_MAP ddd:mm:ssF
gmtset MAP_FRAME_TYPE=fancy MAP_FRAME_PEN=thin MAP_FRAME_WIDTH=2p
gmtset MAP_TICK_LENGTH_PRIMARY=3.5p/2p MAP_TICK_LENGTH_SECONDARY=8p/3p
gmtset MAP_ANNOT_OBLIQUE=32
##########################################################################
############# DEFINE FILENAMES REGIONS PROJECTIONS AND GMT FLAGS #########
##########################################################################
outpng=out.png
DATANAM=${FILENAME}.asc;
REGIONA="-R22.5421/33.004/38.6973/43"; BSMAPA="-Ba1f0.5"
#REGIONA="-R22.5421/33.004/38.6973/42.98632"; BSMAPA="-Ba1f0.5"
REGIONM="-R26.9/30/40.2/41.2"; BSMAPM="-Ba0.5f0.25"
REGIONB="-R28.75/29.2/40.7/41.3"; BSMAPB="-Ba0.2f0.1"
REGIOND="-R25.25/27/39.7/40.6"; BSMAPD="-Ba0.5f0.1"
PROJNON="-JX15/8"; #PROJNON="-Jm0.75i"
PROJGEO="-JX15d/8d"; #PROJGEO="-Jm0.75i"
CONNECT="-Qelem2d.new"
DRAWCOAST="pscoast -R ${PROJGEO} -W0.5p,black -Df -K -O"
##########################################################################
############# PREPARE CPT COLOR PALETTES VARIABLE ATTRIBUTES #############
##########################################################################

if [ ${DATE} = "MEAN" ];then 
	title="2009-2013"  
else
	title=${DATE}
fi

if [ ${VAR} = "BUOY" ];then 
	gmtset COLOR_BACKGROUND=white COLOR_FOREGROUND=black COLOR_NAN=gray
	COL=3; VARNAME="Surface Buoyancy Flux"; UNIT=10@+-7@+m@+2@+s@+-3@+
	OFFSET=0 CONVERT=10000000
	makecpt -Cgray -T-1/1/.1 -D >CPT_A_${EXP}.cpt; TSSB=1
	makecpt -Cgray -T-1/1/.1 -D >CPT_M_${EXP}.cpt; MARB=1
	makecpt -Cgray -T-1/1/.1 -D >CPT_B_${EXP}.cpt; BOSB=1
	makecpt -Cgray -T-1/1/.1 -D >CPT_D_${EXP}.cpt; DARB=1
	makecpt -Cgray -T0/1/.1 -D >CPT_C_${EXP}.cpt; TSSC=1
fi

if [ ${VAR} = "WSTC" ];then 
	gmtset COLOR_BACKGROUND=white COLOR_FOREGROUND=black COLOR_NAN=gray
	COL=3; VARNAME="Wind Stress Curl"; UNIT=10@+-6@+N/m@+3@+
	UNITC=N/m@+2@+
	OFFSET=0 CONVERT=1000000
	makecpt -Chaline -T-32.5/32.5/5 -D >CPT_A_${EXP}.cpt; TSSB=15
	makecpt -Chaline -T-32.5/32.5/5 -D >CPT_M_${EXP}.cpt; MARB=15
	makecpt -Chaline -T-32.5/32.5/5 -D >CPT_B_${EXP}.cpt; BOSB=15
	makecpt -Chaline -T-32.5/32.5/5 -D >CPT_D_${EXP}.cpt; DARB=15
	makecpt -Ccopper -T0/0.1/0.01 -I >CPT_C_${EXP}.cpt; TSSC=0.05
fi

###########################################################################
############# PREPARE INPUT FILES #########################################
###########################################################################
awk '{print $1,$2,($'${COL}'-'${OFFSET}')*'${CONVERT}' }' ${DATANAM} > tss${LAYER}_${EXP}.dat 
if [ ${VAR} = "VEL" ];then 
awk '{print $1,$2,$9}' ${DATANAM} > tss${LAYER}_${EXP}.dat
fi

######################################################################################
############# TURKISH STRAITS SYSTEM #################################################
######################################################################################
fig=${FILENAME}_TSS_${VAR}; ps=${fig}.ps
psbasemap ${REGIONA} ${PROJGEO} ${BSMAPA}wSnE -P -Xc -Yc -K > $ps

pscontour tss${LAYER}_${EXP}.dat ${REGIONA} ${PROJNON} ${CONNECT} ${BSMAPA}wesn -St  -CCPT_A_${EXP}.cpt -I -K -O >> $ps

if [ ${VAR} == "BUOY" ];then 
pscontour tss${LAYER}_${EXP}.dat -R ${PROJNON} ${CONNECT} -B -St -W0.2p -CCPT_C_${EXP}.cpt -Gd1.2i -A0+f8p,Courier-Bold -K -O >> $ps
fi

if [ ${VAR} != "BUOY" ];then 
awk '{print $1,$2,$6 }' ${DATANAM} > stress_${LAYER}_${EXP}.dat 
awk '{print $1,$2,$7,$8}' ${DATANAM} > curr${LAYER}_${EXP}.dat

awk '{print $1,$2,$3}' curr${LAYER}_${EXP}.dat | blockmedian -R -I15.0m -Q >uu_${EXP}
awk '{print $1,$2,$4}' curr${LAYER}_${EXP}.dat | blockmedian -R -I15.0m -Q >uv_${EXP}
paste uu_${EXP} uv_${EXP} > curr_${EXP}.dat
awk '{print $1, $2, (180./3.1416)*atan2($6,$3), 0.1*sqrt($3**2+$6**2)}' curr_${EXP}.dat > curr_lst_${EXP}.dat

pscontour stress_${LAYER}_${EXP}.dat -R ${PROJNON} ${CONNECT} -B -W0.3p -CCPT_C_${EXP}.cpt -Gd0.9i -A+f8p,Courier-Bold -K -O >> $ps
#psscale -D4.15i/0.9i/1.7i/0.075ih -Aal -CCPT_C_${EXP}.cpt -B${TSSC}/:${UNITC}: -K -O >> $ps
psxy curr_lst_${EXP}.dat -R -J -Sv0.02i/0.05i/0.6in0.3 -W0.1p,black -K -O >> $ps

echo "23.03 42.25 1" > contour.line
echo "23.93 42.25 1" >> contour.line
psxy contour.line -R -J -F+jBR -W1p,black -K -O >> $ps
echo "24.00 42.25 Wind Stress (Nm@+-2@+)" | pstext -R ${PROJGEO} -F+jCL+f8p,Courier-Bold,black -O -K -N  >> $ps
echo "23.03 42.00 0 0.5" | psxy -R -J -Sv0.03i/0.05i/0.9in0.3i -F+jCL -W0.1p,black -K -O >> $ps
echo "24.00 42.00 Wind Velocity (5ms@+-1@+)" | pstext -R ${PROJGEO} -F+jCL+f8p,Courier-Bold,black -O -K -N  >> $ps
fi

echo "22.75 42.8 Turkish Straits System" | pstext -R ${PROJGEO} -F+jBL+f11p,Courier-Bold,black -O -K -N  >> $ps
echo "23.03 42.5 ${title}" | pstext -R ${PROJGEO} -F+jBL+f11p,Courier-Bold,black -O -K -N  >> $ps

gmtset FONT_ANNOT_PRIMARY=8p,Courier-Bold,black
gmtset FONT_LABEL=8p,Courier-Bold,black
psscale -D3.75i/0.23i/1.7i/0.075ih -Aal -CCPT_A_${EXP}.cpt -B${TSSB}:"${VARNAME}":/:${UNIT}: -O >> $ps

ps2raster -Tg -V3 -P -F${outpng} $ps 
convert -trim -alpha off -antialias -scale 80% ${outpng} ${fig}.png
rm ${ps} CPT_A_${EXP}.cpt ${outpng}
~/script/put2ftp.sh ${fig}.png . Public/aydogdu/${EXP}/.
#######################################################################################
############## MARMARA SEA ############################################################
#######################################################################################
gmtset FONT_ANNOT_PRIMARY=12p,Courier-Bold,black
gmtset FONT_LABEL=12p,Courier-Bold,black
fig=${FILENAME}_MAR_${VAR}; ps=${fig}.ps
psbasemap ${REGIONM} ${PROJGEO} ${BSMAPM}wSnE -P -Xc -Yc -K > $ps

pscontour tss${LAYER}_${EXP}.dat ${REGIONM} ${PROJNON} ${CONNECT} ${BSMAPM}wesn -St -K -O -CCPT_M_${EXP}.cpt -I >> $ps

if [ ${VAR} == "BUOY" ];then 
pscontour tss${LAYER}_${EXP}.dat -R ${PROJNON} ${CONNECT} -B -St -W0.2p -CCPT_C_${EXP}.cpt -Gd0.9i -A0+f8p,Courier-Bold -K -O >> $ps
#pscontour tss${LAYER}_${EXP}.dat -R ${PROJNON} ${CONNECT} -B -W0.3p -CCPT_C_${EXP}.cpt -Gd0.9i -A+f8p,Courier-Bold -K -O >> $ps
fi
if [ ${VAR} != "BUOY" ];then 
pscontour stress_${LAYER}_${EXP}.dat -R ${PROJNON} ${CONNECT} -B -W0.3p -CCPT_C_${EXP}.cpt -Gd0.75i -A+ap+f8p,Courier-Bold -K -O >> $ps
#psscale -D1.1i/3.0i/1.7i/0.05ih -CCPT_C_${EXP}.cpt -B${TSSC}/:${UNITC}: -K -O >> $ps
psxy curr_lst_${EXP}.dat -R -J -Sv0.03i/0.05i/0.9in0.3i -W0.1p,black -K -O >> $ps
echo "27.03 41.15 1" > contour.line
echo "27.27 41.15 1" >> contour.line
psxy contour.line -R -J -F+jBR -W1p,black -K -O >> $ps
echo "27.32 41.15 Wind Stress (Nm@+-2@+)" | pstext -R ${PROJGEO} -F+jCL+f8p,Courier-Bold,black -O -K -N  >> $ps
echo "27.03 41.08 0 0.5" | psxy -R -J -Sv0.03i/0.05i/0.9in0.3i -F+jCL -W0.1p,black -K -O >> $ps
echo "27.32 41.08 Wind Velocity (5ms@+-1@+)" | pstext -R ${PROJGEO} -F+jCL+f8p,Courier-Bold,black -O -K -N  >> $ps
fi

echo "27.0 40.22 Marmara Sea" | pstext -R ${PROJGEO} -F+jBL+f11p,Courier-Bold,black -O -K -N  >> $ps
echo "29.5 40.22 ${title}" | pstext ${REGIONM} ${PROJGEO} -F+jBL+f11p,Courier-Bold,black -O -K -N  >> $ps
gmtset FONT_ANNOT_PRIMARY=8p,Courier-Bold,black
gmtset FONT_LABEL=8p,Courier-Bold,black
psscale -D3.0i/0.08i/2.0i/0.05ih -Aal -CCPT_M_${EXP}.cpt -B${MARB}:"${VARNAME}":/:${UNIT}: -O >> $ps
ps2raster -Tg -V3 -P -F${outpng} $ps 
convert -trim -alpha off -antialias -scale 80% ${outpng} ${fig}.png
rm ${ps} CPT_M_${EXP}.cpt ${outpng}
~/script/put2ftp.sh ${fig}.png . Public/aydogdu/${EXP}/.
#######################################################################################
############## BOSPHORUS STRAIT #######################################################
#######################################################################################
#fig=${FILENAME}_BOS_${VAR}; ps=${fig}.ps
#psbasemap ${REGIONB} ${PROJGEO} ${BSMAPB}wSnE -P -Xc -Yc -K > $ps
#pscontour tss${LAYER}_${EXP}.dat ${REGIONB} ${PROJNON} ${CONNECT} ${BSMAPB}wesn -St -K -O -Lthin,black -CCPT_B_${EXP}.cpt -I >> $ps
#if [ ${VAR} != "BUOY" ];then 
#awk '{print $1,$2,$3}' curr${LAYER}_${EXP}.dat | blockmedian -R -I1.5m -Q >uu_${EXP}
#awk '{print $1,$2,$4}' curr${LAYER}_${EXP}.dat | blockmedian -R -I1.5m -Q >uv_${EXP}
#paste uu_${EXP} uv_${EXP} > curr_${EXP}.dat
#awk '{print $1, $2, (180./3.1416)*atan2($6,$3), 0.1*sqrt($3**2+$6**2)}' curr_${EXP}.dat > curr_lst_${EXP}.dat
#psxy curr_lst_${EXP}.dat -R -J -Sv0.03i/0.05i/0.9in0.2i -W0.1p -K -O >> $ps
#echo "28.85 41.05 0 0.5" | psxy -R -J -Sv0.03i/0.05i/0.9in0.2i -F+jCL -W0.1p,black -K -O >> $ps
#echo "28.85 41.05 5 m/s" | pstext -R ${PROJGEO} -F+jCR+f12p,Courier-Bold,black -O -K -N  >> $ps
#fi
#echo "28.8 41.25 Bosphorus" | pstext -R ${PROJGEO} -F+jTL+f14p,Courier-Bold,black -O -K -N  >> $ps
#echo "28.75 41.32 ${VARNAME} ${DATE} DEPTH:${LAYER} m." | pstext ${REGIONB} ${PROJGEO} -F+jBL+f13p,Courier-Bold,black -O -K -N  >> $ps
#psscale -D1.6i/2.2i/2.2i/0.075ih -Aal -CCPT_B_${EXP}.cpt -B${BOSB}/:${UNIT}: -O >> $ps
#ps2raster -Tg -V3 -P -F${outpng} $ps 
#convert -trim -alpha off -antialias -scale 80% ${outpng} ${fig}.png
#rm ${ps} CPT_B_${EXP}.cpt ${outpng}
#~/script/put2ftp.sh ${fig}.png . Public/aydogdu/${EXP}/.
#######################################################################################
############## DARDANELLES STRAIT #####################################################
#######################################################################################
#fig=${FILENAME}_DAR_${VAR}; ps=${fig}.ps
#psbasemap ${REGIOND} ${PROJGEO} ${BSMAPD}wSnE -P -Xc -Yc -K > $ps
#pscontour tss${LAYER}_${EXP}.dat ${REGIOND} ${PROJNON} ${CONNECT} ${BSMAPD}wesn -St -K -O -Xc -Yc -Lthin,black -CCPT_D_${EXP}.cpt -I >> $ps
#
#if [ ${VAR} != "BUOY" ];then 
#awk '{print $1,$2,$3}' curr${LAYER}_${EXP}.dat | blockmedian -R -I2.2m -Q >uu_${EXP}
#awk '{print $1,$2,$4}' curr${LAYER}_${EXP}.dat | blockmedian -R -I2.2m -Q >uv_${EXP}
#paste uu_${EXP} uv_${EXP} > curr_${EXP}.dat
#awk '{print $1, $2, (180./3.1416)*atan2($6,$3), 0.1*sqrt($3**2+$6**2)}' curr_${EXP}.dat > curr_lst_${EXP}.dat
#psxy curr_lst_${EXP}.dat -R -J -Sv0.03i/0.05i/0.9in0.2i -W0.1p,black -K -O >> $ps
#echo "26.7 40.00 0 0.5" | psxy -R -J -Sv0.03i/0.05i/0.9in0.2i -F+jCL -W0.1p,black -K -O >> $ps
#echo "26.7 40.00 5 m/s" | pstext -R ${PROJGEO} -F+jCR+f12p,Courier-Bold,black -O -K -N  >> $ps
#fi
#echo "26.5 40.1 Dardanelles" | pstext -R ${PROJGEO} -F+jTL+f14p,Courier-Bold,black -O -K -N  >> $ps
#echo "25.25 40.62 ${VARNAME} ${DATE} DEPTH:${LAYER} m." | pstext ${REGIOND} ${PROJGEO} -F+jBL+f13p,Courier-Bold,black -O -K -N  >> $ps
#psscale -D4.3i/0.25i/1.5i/0.075ih -Aal -CCPT_D_${EXP}.cpt -B${DARB}/:${UNIT}: -O  >> $ps
#ps2raster -Tg -V3 -P -F${outpng} $ps 
#convert -trim -alpha off -antialias -scale 80% ${outpng} ${fig}.png
#~/script/put2ftp.sh ${fig}.png . Public/aydogdu/${EXP}/.
rm ${ps} CPT_D_${EXP}.cpt ${outpng}
#######################################################################################
############# CONVERT OUTPUT POSTSCRIPT FILE ##########################################
#######################################################################################
#echo "25.45 40.75 " | pstext -R ${PROJGEO} -F+a90+jTC+f12p,Courier-Bold,black -O -N  >> $ps
#ps2raster -Tg -V3 -P -F${outpng} $ps 
#convert -trim -alpha off -antialias -scale 80% ${outpng} ${fig}.png
#rm ${ps} CPT_{A,M,B,D}_${EXP}.cpt ${outpng}
#rm tss${LAYER}_${EXP}.dat curr_lst_${EXP}.dat
rm uu_${EXP} uv_${EXP} curr_${EXP}.dat curr${LAYER}_${EXP}.dat
date
exit
