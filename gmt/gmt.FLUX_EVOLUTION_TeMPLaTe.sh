#!/bin/bash
#BSUB -J FEOM_POST         # Name of the job.
#BSUB -o FEOM_POST_%J.out  # Appends std output to file %J.out.
#BSUB -e FEOM_POST_%J.out  # Appends std error to file %J.err.
#BSUB -q serial_30min      # queue
###########################################################################
for VAR in TKE; do
rm gmt.conf
gmtset FONT_ANNOT_PRIMARY=7p,Courier-Bold,black 
gmtset FONT_ANNOT_SECONDARY=8p,Courier-Bold,black FONT_LABEL=8p,Courier-Bold,black
gmtset FORMAT_DATE_MAP "o" 
gmtset FORMAT_TIME_PRIMARY_MAP Abbreviated
gmtset MAP_FRAME_PEN 0.8p,40/40/40 MAP_FRAME_TYPE plain 
gmtset MAP_FRAME_WIDTH 2p MAP_GRID_PEN_PRIMARY 0.1p,black 
gmtset MAP_TICK_LENGTH_PRIMARY 2p/0.5p MAP_TICK_LENGTH_SECONDARY 5p/1.75p
#
RENK=( black red3 aquamarine3 purple4 black paleturquoise4 mediumvioletred tomato blue brown )
PAPER="2.8ix6.2i"
###########################################################################
EXP=BLK02
FLX=( NFX UFX LFX )
FLXNAME=$( echo ${FLX[@]} | sed 's/\ /\_/g' )
FILENAME=FLX
YEARINI=2008; YEAREND=2013; IYR=2008-01-01; FYR=${YEAREND}-12-31; 
XAXSIZE=$( sh CALCDATE.sh 2 ${YEARINI} ${FYR} )
XAXMINI=$( sh CALCDATE.sh 2 ${YEARINI} ${IYR} )
XLAB=$( echo "scale=10; ${XAXSIZE} + 1" | bc ); 

YMIN=-700; YMAX=400; YINC=100; UNITY=km@+3@+y@+-1@+
###########################################################################
if [ ${YEAREND} == ${YEARINI} ]; then 
	TITLE="${YEARINI}"
else
	TITLE="${YEARINI}-${YEAREND}"
fi
fig=${FILENAME}_${FLXNAME}_${YEAREND}; ps=${fig}.ps
REGIONT="$IYR"T/"$FYR"T/$YMIN/$YMAX; PROJT="X12T/5";
REGIONS=${XAXMINI}/${XAXSIZE}/$YMIN/$YMAX; PROJS="X12/5";
REGIONM=1/72/$YMIN/$YMAX; PROJS="X12/5";

if [ ${YEAREND} = 2008 ];then 
	BASE1T=pa1Og1Of1r:"MONTH":/${YINC}f${YTICK}:${UNITY}:/WSne; BASE2T=sa1YS
elif [ ${YEAREND} != 2008 ];then 
	BASE1T=pa1Yg1Yf1o:"YEAR":/${YINC}f${YTICK}:${UNITY}:/WSne; BASE2T=sa1YS
fi

psbasemap -R$REGIONT -J$PROJT -B$BASE1T --PS_MEDIA=$PAPER -K -Xc -Yc > $ps
sig=1
SYM=(  ___ --- ... .-. )
PEN=(  0.5p faint thin thick fat thin )
PEN2=( 2.0p faint thin thick fat thin )
RENK=( white black darkblue darkred  blue green gold cyan springgreen purple4)
RENK2=( white gray lightblue lightred green gold cyan springgreen purple4)

###########################################################################
i=0; while [ ${i} -lt ${#FLX[@]} ]; do

if [[ "${FLX[${i}]}" = NFX ]]; then
COL=2; VARNAME="Net Flux";
elif [[ "${FLX[${i}]}" = UFX ]]; then
COL=3; VARNAME="Upper Layer Flux";
elif [[ "${FLX[${i}]}" = LFX ]]; then
COL=4; VARNAME="Lower Layer Flux";
fi

#XLAB=$( echo "scale=10; -0.5 + ${i} * (5/${#FLX[@]})" | bc );
YLAB=$( echo "scale=15; ${YMIN} - ${YMIN}/20 + ${YINC}*3.5*${i}/10" | bc );
cat ${FILENAME}_${EXP}_20{08,09,10,11,12,13}.lst > ${FILENAME}${EXP}.asc
PCOLOR=$[ ${i} + 1 ]
wc -l ${FILENAME}${EXP}.asc
cat -n ${FILENAME}${EXP}.asc | awk '{print $1,$'${COL}'}' \
       | gmtconvert -o0,1 -f0t > PLTDAT.dat
psxy PLTDAT.dat -R$REGIONS -W${PEN[0]},${RENK2[${PCOLOR}]},${SYM[0]} -J$PROJS -B:'': -O -K -Xc -Yc>> $ps
echo "$(echo ${XLAB}/85 |bc ) ${YLAB} ${SYM[0]}" | \
   pstext -R$REGIONS -G${RENK2[${PCOLOR}]} -W0.3p,${RENK[${PCOLOR}]} -F+jLM+f8p,Courier-Bold,${RENK[${PCOLOR}]} -N -J$PROJS -B:'': -O -K -Xc -Yc>> $ps
echo "$(echo ${XLAB}/17 |bc ) ${YLAB} ${VARNAME}" | \
   pstext -R$REGIONS -F+jLM+f6p,Courier,${RENK[${PCOLOR}]} -N -J$PROJS -B:'': -O -K -Xc -Yc>> $ps
i=$[ ${i} + 1 ]; 
done

i=0; while [ ${i} -lt ${#FLX[@]} ]; do

if [[ "${FLX[${i}]}" = NFX ]]; then
COL=2; VARNAME="NET FLUX";
elif [[ "${FLX[${i}]}" = UFX ]]; then
COL=3; VARNAME="UPPER LAYER FLUX";
elif [[ "${FLX[${i}]}" = LFX ]]; then
COL=4; VARNAME="LOWER LAYER FLUX";
fi

if [ ${i} = 0 ];then XLABEL=${XAXMINI}; XORI=RM;fi
if [ ${i} = 1 ];then XLABEL=${XAXSIZE}; XORI=LM;fi
YLAB=$( echo "scale=15; ${YMAX} - ${YINC}*3*${i}/4" | bc );
cat ${FILENAME}_${EXP}_20{08,09,10,11,12,13}.lst > ${FILENAME}${EXP}.asc
wc -l ${FILENAME}${EXP}.asc
cat -n ${FILENAME}${EXP}.asc | awk '{print $1,$'${COL}'}' \
       | gmtconvert -o0,1 -f0t > PLTDAT.dat
MEAN=$( cat PLTDAT.dat | awk '{ sum += $2; n++ } END { if (n > 0) print sum / n; }' )
PCOLOR=$[ ${i} + 1 ]
echo "1 ${MEAN}" > MEAN.dat
echo "${XAXSIZE} ${MEAN}" >> MEAN.dat
psxy MEAN.dat -R$REGIONS -W${PEN[0]},${RENK[${PCOLOR}]},${SYM[0]} -J$PROJS -B:'': -O -K -Xc -Yc>> $ps
cat -n ${FILENAME}_${EXP}_MONTHLY_MEAN.asc | awk '{print $1,$'${COL}'}' \
       | gmtconvert -o0,1 -f0t > PLTDAT.dat
psxy PLTDAT.dat -R$REGIONM -W${PEN[0]},${RENK[${PCOLOR}]},${SYM[0]} -J$PROJS -B:'': -O -K -Xc -Yc>> $ps
echo "${XLABEL} ${MEAN} $(echo ${MEAN} | xargs printf "%.*f\n" $sig )" | pstext -R$REGIONS -F+j${XORI}+f6p,Courier-Bold,${RENK[${PCOLOR}]} -N -J$PROJS -B:'': -O -K -Xc -Yc>> $ps
i=$[ ${i} + 1 ]; done
done
echo " ${XAXSIZE} ${YMAX} Southern Bosphorus" | pstext -R${REGIONS} -J${PROJS} -K -O -F+jRT+f8p,Courier-Bold,black -Xc -Yc>> $ps
pslegend -R -J -Dx0.5i/2.5i/3.2i/0.1/BL -C0.2i/0.2i -O -Y-0.5 >> $ps <<EOF
G -0.1i
H 10 Courier-Bold 
N 7
EOF
ps2raster -Tg -P -Foutput $ps; sh convert.ps2jpg.sh ${fig}
~/script/put2ftp.sh ${fig}.png . Public/aydogdu/.
