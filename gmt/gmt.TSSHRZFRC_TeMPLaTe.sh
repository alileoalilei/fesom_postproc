#!/bin/bash
###########################################################################################################
############# GMT SCRIPT TO PLOT HORIZONTAL SECTIONS FOR TSS ##############################################
############# MODIFY DATE AND LAYER BY A SUBMIT SCRIPT USING SED ##########################################
###########################################################################################################
### written by: ali aydogdu ###############################################################################
### last modified: 2015/01/27 #############################################################################
###########################################################################################################
date
###########################################################################################################
############# SET PARAMETERS FROM SUBMIT SCRIPT ###########################################################
###########################################################################################################
LAYER=`echo LAYERNUMBER | awk '{printf("%02d\n",$1)}'`
YEAR=YEAR2PLOT
DAY=`echo DAY2PLOT | awk '{printf("%03d\n",$1)}'`
DATE=$(echo ${DAY} | awk '{printf "%d\n",$0;}')
DATE=$(sh ~/script/sh/CALCDATE.sh ${DATE} ${YEAR})
VAR=VARIABLE2PLOT
EXP=EXPCODE
###########################################################################################################
############# SET GMT DEFAULTS ############################################################################
###########################################################################################################
gmtset PS_MEDIA=20ix12i
gmtset FONT_ANNOT_PRIMARY=18p,Helvetica,black FONT_ANNOT_SECONDARY=18p,Helvetica,black FONT_LABEL=18p,Helvetica,black
###########################################################################################################
############# DEFINE FILENAMES REGIONS PROJECTIONS AND GMT FLAGS ##########################################
###########################################################################################################
DATANAM="TEMPSALTCURR${LAYER}m_${EXP}.lst";
REGIONA="-R22.5421/33.004/38.6973/42.98632"; BSMAPA="-Ba1f0.5WsNe -P -St"
REGIONM="-R26.85/30/40.20/41.1"; BSMAPM="-Ba0.4f0.1wsNE -P -St"
REGIONB="-R28.75/29.25/40.75/41.3"; BSMAPB="-Ba0.2f0.1WSne -P -St"
REGIOND="-R26/27/39.75/40.75"; BSMAPD="-Ba0.2f0.1wSnE -P -St"
PROJNON="-JX22.5/12"; #PROJNON="-Jm0.75i"
PROJGEO="-JX22.5d/12d"; #PROJGEO="-Jm0.75i"
CONNECT="-Qelem2d.new"
DRAWCOAST="pscoast -R ${PROJGEO} -W0.5p,black -Df -K -O -V3"
###########################################################################################################
############# PREPARE CPT COLOR PALETTES VARIABLE ATTRIBUTES ##############################################
###########################################################################################################
if [ ${VAR} = "RELAX_SALT" ];then 
	gmtset COLOR_BACKGROUND=black COLOR_FOREGROUND=white COLOR_NAN=gray
	COL=3; VARNAME="SALINITY RELAXATION"; FIGNAME=SRLX
		makecpt -Csplitblue -T-0.000001/0.000001/0.00000001 >CPT_A_LEV01.cpt; TSSB=0.000001
		makecpt -Csplitblue -T-0.000001/0.000001/0.00000001 >CPT_M_LEV01.cpt; MARB=0.000001
		makecpt -Csplitblue -T-0.000001/0.000001/0.00000001 >CPT_B_LEV01.cpt; BOSB=0.000001
		makecpt -Csplitblue -T-0.000001/0.000001/0.00000001 >CPT_D_LEV01.cpt; DARB=0.000001
fig1=TSS_${EXP}_${FIGNAME}_${YEAR}_DAY${DAY}_LEV${LAYER}
elif [ ${VAR} = "RUNOFF" ];then 
	gmtset COLOR_BACKGROUND=black COLOR_FOREGROUND=white COLOR_NAN=gray
	COL=4; VARNAME="BLACK SEA BUDGET"; FIGNAME=BLKR
		makecpt -Csplitblue -T-0.000001/0.000001/0.0000001 >CPT_A_LEV01.cpt; TSSB=0.0000005
		makecpt -Csplitblue -T-0.000001/0.000001/0.0000001 >CPT_M_LEV01.cpt; MARB=0.0000005
		makecpt -Csplitblue -T-0.000001/0.000001/0.0000001 >CPT_B_LEV01.cpt; BOSB=0.0000005
		makecpt -Csplitblue -T-0.000001/0.000001/0.0000001 >CPT_D_LEV01.cpt; DARB=0.0000005
fig1=TSS_${EXP}_${FIGNAME}_${YEAR}_DAY${DAY}_LEV${LAYER}
elif [ ${VAR} = "WNET" ];then 
	gmtset COLOR_BACKGROUND=black COLOR_FOREGROUND=white COLOR_NAN=gray
	COL=5; VARNAME="VERTICAL VELOCITY"; FIGNAME=WNET
		makecpt -Csplitblue -T-0.000001/0.000001/0.0000001 >CPT_A_LEV01.cpt; TSSB=0.0000005
		makecpt -Csplitblue -T-0.000001/0.000001/0.0000001 >CPT_M_LEV01.cpt; MARB=0.0000005
		makecpt -Csplitblue -T-0.000001/0.000001/0.0000001 >CPT_B_LEV01.cpt; BOSB=0.0000005
		makecpt -Csplitblue -T-0.000001/0.000001/0.0000001 >CPT_D_LEV01.cpt; DARB=0.0000005
fig1=TSS_${EXP}_${FIGNAME}_${YEAR}_DAY${DAY}_LEV${LAYER}
elif [ ${VAR} = "EVAP" ];then 
	gmtset COLOR_BACKGROUND=black COLOR_FOREGROUND=white COLOR_NAN=gray
	COL=6; VARNAME="EVAPORATION"; FIGNAME=EVAP
		makecpt -Csplitblue -T-0.000001/0.000001/0.0000001 >CPT_A_LEV01.cpt; TSSB=0.0000005
		makecpt -Csplitblue -T-0.000001/0.000001/0.0000001 >CPT_M_LEV01.cpt; MARB=0.0000005
		makecpt -Csplitblue -T-0.000001/0.000001/0.0000001 >CPT_B_LEV01.cpt; BOSB=0.0000005
		makecpt -Csplitblue -T-0.000001/0.000001/0.0000001 >CPT_D_LEV01.cpt; DARB=0.0000005
fig1=TSS_${EXP}_${FIGNAME}_${YEAR}_DAY${DAY}_LEV${LAYER}
else 
	echo "VARIABLE is not define for makecpt"
fi
ps=${fig1}.ps

###########################################################################################################
############# PREPARE INPUT FILES #########################################################################
###########################################################################################################
awk '{print $1,$2,$'${COL}'}' ${DATANAM} > tss${LAYER}.dat 
#awk '{print $1,$2,$5,$6}' ${DATANAM} > curr${LAYER}.dat
###########################################################################################################
############# TURKISH STRAITS SYSTEM ######################################################################
###########################################################################################################
pscontour tss${LAYER}.dat ${REGIONA} ${PROJNON} ${CONNECT} ${BSMAPA} -K -Y6.5i -Lthin,black -CCPT_A_LEV${LAYER}.cpt -I -V3 > $ps
#awk '{print $1,$2,$3}' curr${LAYER}.dat | blockmedian -R -I6.0m -Q >uu
#awk '{print $1,$2,$4}' curr${LAYER}.dat | blockmedian -R -I6.0m -Q >uv
#paste uu uv > curr.dat
#awk '{print $1, $2, (180./3.1416)*atan2($6,$3), sqrt($3**2+$6**2)}' curr.dat > curr_lst.dat
#psxy curr_lst.dat -R -J -Sv0.03/0.06/0.9 -W0.1p,black -K -O >> $ps
#echo "31.1 39.50 0 0.5" | psxy -R -J -Sv0.03/0.06/0.9 -F+jCL -W0.1p,black -K -O >> $ps
#echo "31.1 39.50 0.5 m/s" | pstext -R ${PROJGEO} -F+jCR+f18p,Helvetica,-=1.0p,black -O -K -N  >> $ps
echo "23 42.5 Turkish Straits System" | pstext -R ${PROJGEO} -F+jTL+f24p,Helvetica,-=1.0p,red -O -K -N  >> $ps
psscale -D6.2i/0.35i/4i/0.25ih -Aal --D_FORMAT=%e -CCPT_A_LEV${LAYER}.cpt -B${TSSB} -E -O -K >> $ps
###########################################################################################################
############# MARMARA SEA #################################################################################
###########################################################################################################
pscontour tss${LAYER}.dat ${REGIONM} ${PROJNON} ${CONNECT} ${BSMAPM} -K -O -X9.5i -Lthin,black -CCPT_M_LEV${LAYER}.cpt -I -V3 >> $ps
#awk '{print $1,$2,$3}' curr${LAYER}.dat | blockmedian -R -I2.5m -Q >uu
#awk '{print $1,$2,$4}' curr${LAYER}.dat | blockmedian -R -I2.5m -Q >uv
#paste uu uv > curr.dat
#awk '{print $1, $2, (180./3.1416)*atan2($6,$3), 0.6*sqrt($3**2+$6**2)}' curr.dat > curr_lst.dat
#psxy curr_lst.dat -R -J -Sv0.03i/0.06i/0.9in0.3i -W0.1p,black -K -O >> $ps
#echo "29.5 40.35 0 0.3" | psxy -R -J -Sv0.03i/0.06i/0.9in0.3i -F+jCL -W0.1p,black -K -O >> $ps
#echo "29.5 40.35 0.5 m/s" | pstext -R ${PROJGEO} -F+jCR+f18p,Helvetica,-=1.0p,black -O -K -N  >> $ps
echo "26.9 40.25 Marmara Sea" | pstext -R ${PROJGEO} -F+jBL+f24p,Helvetica,-=1.0p,red -O -K -N  >> $ps
psscale -D6.2i/0.35i/4i/0.25ih -Aal --D_FORMAT=%e -CCPT_M_LEV${LAYER}.cpt -B${MARB} -E -O -K -V3 >> $ps
###########################################################################################################
############# BOSPHORUS STRAIT ############################################################################
###########################################################################################################
pscontour tss${LAYER}.dat ${REGIONB} ${PROJNON} ${CONNECT} ${BSMAPB} -K -O -Y-5i -X-9.5i -Lthin,black -CCPT_B_LEV${LAYER}.cpt -I -V3 >> $ps
#awk '{print $1,$2,$3}' curr${LAYER}.dat | blockmedian -R -I0.6m -Q >uu
#awk '{print $1,$2,$4}' curr${LAYER}.dat | blockmedian -R -I0.6m -Q >uv
#paste uu uv > curr.dat
#awk '{print $1, $2, (180./3.1416)*atan2($6,$3), 0.6*sqrt($3**2+$6**2)}' curr.dat > curr_lst.dat
#psxy curr_lst.dat -R -J -Sv0.03i/0.06i/0.9in0.2i -W0.1p -K -O -V3 >> $ps
#echo "28.85 41.05 0 0.3" | psxy -R -J -Sv0.03i/0.06i/0.9in0.2i -F+jCL -W0.1p,black -K -O >> $ps
#echo "28.85 41.05 0.5 m/s" | pstext -R ${PROJGEO} -F+jCR+f18p,Helvetica,-=1.0p,black -O -K -N  >> $ps
echo "28.8 41.2 Bosphorus" | pstext -R ${PROJGEO} -F+jTL+f24p,Helvetica,-=1.0p,red -O -K -N  >> $ps
psscale -D2i/3.2i/3i/0.25ih -Aal --D_FORMAT=%e -CCPT_B_LEV${LAYER}.cpt -B${BOSB} -E -O -K -V >> $ps
###########################################################################################################
############# DARDANELLES STRAIT ##########################################################################
###########################################################################################################
pscontour tss${LAYER}.dat ${REGIOND} ${PROJNON} ${CONNECT} ${BSMAPD} -K -O -X9.5i -Lthin,black -CCPT_D_LEV${LAYER}.cpt -I -V3 >> $ps
#awk '{print $1,$2,$3}' curr${LAYER}.dat | blockmedian -R -I1.3m -Q >uu
#awk '{print $1,$2,$4}' curr${LAYER}.dat | blockmedian -R -I1.3m -Q >uv
#paste uu uv > curr.dat
#awk '{print $1, $2, (180./3.1416)*atan2($6,$3), 0.6*sqrt($3**2+$6**2)}' curr.dat > curr_lst.dat
##psxy curr_lst.dat -R -J -Sv0.03i/0.06i/0.9in0.2i -W0.1p,black -K -O >> $ps
#echo "26.9 40.00 0 0.3" | psxy -R -J -Sv0.03i/0.06i/0.9in0.2i -F+jCL -W0.1p,black -K -O >> $ps
#echo "26.9 40.00 0.5 m/s" | pstext -R ${PROJGEO} -F+jCR+f18p,Helvetica,-=1.0p,black -O -K -N  >> $ps
echo "26.5 40.0 Dardanelles" | pstext -R ${PROJGEO} -F+jTL+f24p,Helvetica,-=1.0p,red -O -K -N  >> $ps
psscale -D6.2i/0.35i/4i/0.25ih --D_FORMAT=%e -Aal -CCPT_D_LEV${LAYER}.cpt -B${DARB} -E -O -K >> $ps
###########################################################################################################
############# CONVERT OUTPUT POSTSCRIPT FILE ##############################################################
###########################################################################################################
echo "25.95 40.75 ${VARNAME} ${DATE} LAYER:${LAYER} EXP:${EXP}" | pstext -R ${PROJGEO} -F+a90+jTC+f24p,Helvetica,-=1.0p,black -O -N  >> $ps
ps2raster -TG -V3 -Foutput_${EXP} $ps 
date
sh convert.ps2jpg.sh ${fig1} ${EXP}
#rm $ps
date
###########################################################################################################
######## TO DO: ###########################################################################################
############# FIND A WAY TO CONTOUR NaN VALUES INSTEAD OF 0000 IN FORTRAN OUTPUT ##########################
############# CONTOUR BOTH TEMPERATURE AND SALINITY #######################################################
############# CONTOUR OTHER OUTPUT PARAMETERS #############################################################
############# PREPARE CPT IN A GENERIC WAY FOR EACH LAYER #################################################
############# PUT LABELS TO THE FIGURES INCLUDING THE DATE ################################################
###########################################################################################################
exit
