#!/bin/bash
###########################################################################################################
############# GMT SCRIPT TO PLOT HORIZONTAL SECTIONS FOR TSS ##############################################
############# MODIFY DATE AND LAYER BY A SUBMIT SCRIPT USING SED ##########################################
###########################################################################################################
### written by: ali aydogdu ###############################################################################
### last modified: 2015/01/27 #############################################################################
###########################################################################################################
date
###########################################################################################################
############# SET PARAMETERS FROM SUBMIT SCRIPT ###########################################################
###########################################################################################################
LAYER=`echo 25 | awk '{printf("%03d\n",$1)}'`
DAY=$( echo "(13 - 1)*5+1"| bc | awk '{printf("%03d\n",$1)}' )
YEAR=2009
DATE=$(echo ${DAY} | awk '{printf "%d\n",$0;}')
DATE=$(sh ~/script/sh/CALCDATE.sh 1 ${YEAR} ${DATE})
VAR=SAL
EXP=ZEN02
DATADIR="ENSMEAN"
FILENAME=${DATADIR}/ENSMEAN_${EXP}_STDEV_${YEAR}_DAY${DAY}_LEV${LAYER};
###########################################################################################################
############# SET GMT DEFAULTS ############################################################################
###########################################################################################################
gmtset PS_MEDIA=20ix14i IO_NAN_RECORDS=pass PLOT_DEGREE_FORMAT ddd:mm:ssF
gmtset FONT_ANNOT_PRIMARY=15p,Helvetica,black FONT_ANNOT_SECONDARY=15p,Helvetica,black FONT_LABEL=15p,Helvetica,black
###########################################################################################################
############# DEFINE FILENAMES REGIONS PROJECTIONS AND GMT FLAGS ##########################################
###########################################################################################################
fig=${FILENAME}_${VAR}; ps=${fig}.ps
outpng=${DATADIR}/out.png
DATANAM=${FILENAME}.asc;
REGIONA="-R22.5421/33.004/38.6973/42.98632"; BSMAPA="-Ba1f0.5WsNe -P -St"
REGIONM="-R26.85/30/40.20/41.2"; BSMAPM="-Ba0.4f0.1wsNE -P -St"
REGIONB="-R28.75/29.25/40.75/41.3"; BSMAPB="-Ba0.2f0.1WSne -P -St"
REGIOND="-R26/27/39.75/40.75"; BSMAPD="-Ba0.2f0.1wSnE -P -St"
PROJNON="-JX15/8"; #PROJNON="-Jm0.75i"
PROJGEO="-JX15d/8d"; #PROJGEO="-Jm0.75i"
CONNECT="-Qelem2d.new"
DRAWCOAST="pscoast -R ${PROJGEO} -W0.5p,black -Df -K -O -V3"
###########################################################################################################
############# PREPARE CPT COLOR PALETTES VARIABLE ATTRIBUTES ##############################################
###########################################################################################################
if [ ${VAR} = "SAL" ];then 
	gmtset COLOR_BACKGROUND=white COLOR_FOREGROUND=black COLOR_NAN=gray
	COL=4; VARNAME=SALINITY
	if [ ${LAYER} -ge 1 ] && [ ${LAYER} -lt 20 ];then
		makecpt -Csss -T0/0.01/0.0001  >CPT_A_${EXP}.cpt; TSSB=0.005
		makecpt -Csss -T0/0.001/0.00001  >CPT_M_${EXP}.cpt; MARB=0.0005
		makecpt -Csss -T0/0.001/0.00001  >CPT_B_${EXP}.cpt; BOSB=0.0005
		makecpt -Csss -T0/0.001/0.00001  >CPT_D_${EXP}.cpt; DARB=0.0005
	elif [ ${LAYER} -ge 20 ] && [ ${LAYER} -lt 40 ];then
		makecpt -Csss -T0/0.01/0.0001  >CPT_A_${EXP}.cpt; TSSB=0.005
		makecpt -Csss -T0/0.001/0.00001  >CPT_M_${EXP}.cpt; MARB=0.0005
		makecpt -Csss -T0/0.001/0.00001  >CPT_B_${EXP}.cpt; BOSB=0.0005
		makecpt -Csss -T0/0.001/0.00001  >CPT_D_${EXP}.cpt; DARB=0.0005
	elif [ ${LAYER} -ge 40 ] && [ ${LAYER} -lt 60 ];then
		makecpt -Csss -T0/0.001/0.00001  >CPT_A_${EXP}.cpt; TSSB=0.0005
		makecpt -Csss -T0/0.001/0.00001  >CPT_M_${EXP}.cpt; MARB=0.0005
		makecpt -Csss -T0/0.001/0.00001  >CPT_B_${EXP}.cpt; BOSB=0.0005
		makecpt -Csss -T0/0.001/0.00001  >CPT_D_${EXP}.cpt; DARB=0.0005
	else 
		echo "LAYER is not available for makecpt"
	fi
elif [ ${VAR} = "TEM" ];then 
	gmtset COLOR_BACKGROUND=white COLOR_FOREGROUND=black COLOR_NAN=gray
	COL=3; VARNAME=TEMPERATURE
	if [ ${LAYER} -ge 1 ] && [ ${LAYER} -lt 20 ];then
		makecpt -Csst -T0/0.01/0.0001 >CPT_A_${EXP}.cpt; TSSB=0.005
		makecpt -Csst -T0/0.001/0.00001 >CPT_M_${EXP}.cpt; MARB=0.0005
		makecpt -Csst -T0/0.001/0.00001 >CPT_B_${EXP}.cpt; BOSB=0.0005
		makecpt -Csst -T0/0.001/0.00001 >CPT_D_${EXP}.cpt; DARB=0.0005
	elif [ ${LAYER} -ge 20 ] && [ ${LAYER} -lt 40 ];then
		makecpt -Csst -T0/0.01/0.0001 >CPT_A_${EXP}.cpt; TSSB=0.005
		makecpt -Csst -T0/0.001/0.00001 >CPT_M_${EXP}.cpt; MARB=0.0005
		makecpt -Csst -T0/0.001/0.00001 >CPT_B_${EXP}.cpt; BOSB=0.0005
		makecpt -Csst -T0/0.001/0.00001 >CPT_D_${EXP}.cpt; DARB=0.0005
	elif [ ${LAYER} -ge 40 ] && [ ${LAYER} -lt 60 ];then
		makecpt -Csst -T0/0.01/0.0001    >CPT_A_${EXP}.cpt; TSSB=0.005
		makecpt -Csst -T0/0.001/0.00001 >CPT_M_${EXP}.cpt; MARB=0.0005
		makecpt -Csst -T0/0.001/0.00001 >CPT_B_${EXP}.cpt; BOSB=0.0005
		makecpt -Csst -T0/0.001/0.00001 >CPT_D_${EXP}.cpt; DARB=0.0005
	else 
		echo "LAYER is not available for makecpt"
	fi
elif [ ${VAR} = "SSH" ];then 
	gmtset COLOR_BACKGROUND=white COLOR_FOREGROUND=black COLOR_NAN=gray
	COL=7; VARNAME=ELEVATION
	if [ ${LAYER} -eq 1 ];then
		makecpt -Csst -T0/0.001/0.00001  >CPT_A_${EXP}.cpt; TSSB=0.0005
		makecpt -Csst -T0/0.001/0.00001  >CPT_M_${EXP}.cpt; MARB=0.0005
		makecpt -Csst -T0/0.001/0.00001  >CPT_B_${EXP}.cpt; BOSB=0.0005
		makecpt -Csst -T0/0.001/0.00001  >CPT_D_${EXP}.cpt; DARB=0.0005
	else 
		echo "LAYER is not available for makecpt"
	fi
else 
	echo "VARIABLE is not define for makecpt"
fi

###########################################################################################################
############# PREPARE INPUT FILES #########################################################################
###########################################################################################################
awk '{print $1,$2,$'${COL}'}' ${DATANAM} > tss${LAYER}_${EXP}.dat 
awk '{print $1,$2,$5,$6}' ${DATANAM} > curr${LAYER}_${EXP}.dat
###########################################################################################################
############# TURKISH STRAITS SYSTEM ######################################################################
###########################################################################################################
pscontour tss${LAYER}_${EXP}.dat ${REGIONA} ${PROJNON} ${CONNECT} ${BSMAPA} -K -Yc -Xc -Lthin,black -CCPT_A_${EXP}.cpt -I -V3 > $ps
awk '{print $1,$2,$3}' curr${LAYER}_${EXP}.dat | blockmedian -R -I6.0m -Q >uu_${EXP}
awk '{print $1,$2,$4}' curr${LAYER}_${EXP}.dat | blockmedian -R -I6.0m -Q >uv_${EXP}
paste uu_${EXP} uv_${EXP} > curr_${EXP}.dat
awk '{print $1, $2, (180./3.1416)*atan2($6,$3), 0.8*sqrt($3**2+$6**2)}' curr_${EXP}.dat > curr_lst_${EXP}.dat
psxy curr_lst_${EXP}.dat -R -J -Sv0.02i/0.05i/0.6in0.3 -W0.1p,black -K -O >> $ps
echo "31.1 39.50 0 0.4" | psxy -R -J -Sv0.02i/0.05i/0.6in0.3 -F+jCL -W0.1p,black -K -O >> $ps
echo "31.1 39.50 0.5 m/s" | pstext -R ${PROJGEO} -F+jCR+f15p,Helvetica,-=1.0p,black -O -K -N  >> $ps
echo "22.75 42.5 Turkish Straits System" | pstext -R ${PROJGEO} -F+jTL+f20p,Helvetica,-=1.0p,red -O -K -N  >> $ps
psscale -D4.15i/0.23i/3i/0.1ih -Aal -CCPT_A_${EXP}.cpt -B${TSSB} -E -O -K >> $ps
###########################################################################################################
############# MARMARA SEA #################################################################################
###########################################################################################################
pscontour tss${LAYER}_${EXP}.dat ${REGIONM} ${PROJNON} ${CONNECT} ${BSMAPM} -K -O -X6.3i -Lthin,black -CCPT_M_${EXP}.cpt -I -V3 >> $ps
awk '{print $1,$2,$3}' curr${LAYER}_${EXP}.dat | blockmedian -R -I2.8m -Q >uu_${EXP}
awk '{print $1,$2,$4}' curr${LAYER}_${EXP}.dat | blockmedian -R -I2.8m -Q >uv_${EXP}
paste uu_${EXP} uv_${EXP} > curr_${EXP}.dat
awk '{print $1, $2, (180./3.1416)*atan2($6,$3), 0.4*sqrt($3**2+$6**2)}' curr_${EXP}.dat > curr_lst_${EXP}.dat
psxy curr_lst_${EXP}.dat -R -J -Sv0.03i/0.05i/0.9in0.3i -W0.1p,black -K -O >> $ps
echo "29.5 40.38 0 0.2" | psxy -R -J -Sv0.03i/0.05i/0.9in0.3i -F+jCL -W0.1p,black -K -O >> $ps
echo "29.5 40.38 0.5 m/s" | pstext -R ${PROJGEO} -F+jCR+f15p,Helvetica,-=1.0p,black -O -K -N  >> $ps
echo "26.9 40.22 Marmara Sea" | pstext -R ${PROJGEO} -F+jBL+f20p,Helvetica,-=1.0p,red -O -K -N  >> $ps
psscale -D4.1i/0.21i/3i/0.1ih -Aal -CCPT_M_${EXP}.cpt -B${MARB} -E -O -K -V3 >> $ps
###########################################################################################################
############# BOSPHORUS STRAIT ############################################################################
###########################################################################################################
pscontour tss${LAYER}_${EXP}.dat ${REGIONB} ${PROJNON} ${CONNECT} ${BSMAPB} -K -O -Y-3.5i -X-6.3i -Lthin,black -CCPT_B_${EXP}.cpt -I -V3 >> $ps
awk '{print $1,$2,$3}' curr${LAYER}_${EXP}.dat | blockmedian -R -I1.5m -Q >uu_${EXP}
awk '{print $1,$2,$4}' curr${LAYER}_${EXP}.dat | blockmedian -R -I1.5m -Q >uv_${EXP}
paste uu_${EXP} uv_${EXP} > curr_${EXP}.dat
awk '{print $1, $2, (180./3.1416)*atan2($6,$3), 0.3*sqrt($3**2+$6**2)}' curr_${EXP}.dat > curr_lst_${EXP}.dat
psxy curr_lst_${EXP}.dat -R -J -Sv0.03i/0.05i/0.9in0.2i -W0.1p -K -O -V3 >> $ps
echo "28.85 41.05 0 0.15" | psxy -R -J -Sv0.03i/0.05i/0.9in0.2i -F+jCL -W0.1p,black -K -O >> $ps
echo "28.85 41.05 0.5 m/s" | pstext -R ${PROJGEO} -F+jCR+f15p,Helvetica,-=1.0p,black -O -K -N  >> $ps
echo "28.8 41.25 Bosphorus" | pstext -R ${PROJGEO} -F+jTL+f20p,Helvetica,-=1.0p,red -O -K -N  >> $ps
psscale -D1.6i/2.2i/3.0i/0.1ih -Aal -CCPT_B_${EXP}.cpt -B${BOSB} -E -O -K -V >> $ps
###########################################################################################################
############# DARDANELLES STRAIT ##########################################################################
###########################################################################################################
pscontour tss${LAYER}_${EXP}.dat ${REGIOND} ${PROJNON} ${CONNECT} ${BSMAPD} -K -O -X6.3i -Lthin,black -CCPT_D_${EXP}.cpt -I -V3 >> $ps
awk '{print $1,$2,$3}' curr${LAYER}_${EXP}.dat | blockmedian -R -I2.2m -Q >uu_${EXP}
awk '{print $1,$2,$4}' curr${LAYER}_${EXP}.dat | blockmedian -R -I2.2m -Q >uv_${EXP}
paste uu_${EXP} uv_${EXP} > curr_${EXP}.dat
awk '{print $1, $2, (180./3.1416)*atan2($6,$3), 0.3*sqrt($3**2+$6**2)}' curr_${EXP}.dat > curr_lst_${EXP}.dat
psxy curr_lst_${EXP}.dat -R -J -Sv0.03i/0.05i/0.9in0.2i -W0.1p,black -K -O >> $ps
echo "26.9 40.00 0 0.15" | psxy -R -J -Sv0.03i/0.05i/0.9in0.2i -F+jCL -W0.1p,black -K -O >> $ps
echo "26.9 40.00 0.5 m/s" | pstext -R ${PROJGEO} -F+jCR+f15p,Helvetica,-=1.0p,black -O -K -N  >> $ps
echo "26.5 40.1 Dardanelles" | pstext -R ${PROJGEO} -F+jTL+f20p,Helvetica,-=1.0p,red -O -K -N  >> $ps
psscale -D4.1i/0.25i/3i/0.1ih -Aal -CCPT_D_${EXP}.cpt -B${DARB} -E -O -K >> $ps
###########################################################################################################
############# CONVERT OUTPUT POSTSCRIPT FILE ##############################################################
###########################################################################################################
echo "25.95 40.75 ${VARNAME} ${DATE} DEPTH:${LAYER} m." | pstext -R ${PROJGEO} -F+a90+jTC+f20p,Helvetica,-=1.0p,black -O -N  >> $ps
ps2raster -Tg -V3 -P -F${outpng} $ps 
convert -trim -alpha off -antialias -scale 80% ${outpng} ${fig}.png
rm ${ps} CPT_{A,M,B,D}_${EXP}.cpt ${outpng}
rm tss${LAYER}_${EXP}.dat curr_lst_${EXP}.dat
rm uu_${EXP} uv_${EXP} curr_${EXP}.dat curr${LAYER}_${EXP}.dat
date
exit
