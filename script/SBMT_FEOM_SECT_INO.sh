#!/bin/bash
#BSUB -a poe               #
#BSUB -J FEOM_POST         # Name of the job.
#BSUB -o FEOM_POST_%J.out  # Appends std output to file %J.out.
#BSUB -e FEOM_POST_%J.out  # Appends std error to file %J.err.
#BSUB -P fesom             # Project ID.
#BSUB -q serial_30min        # queue
####BSUB -x                      
TOOLCODE=9
INITIALDAY=1; FINALDAY=1
EXPDEF=INI; EXPNUM=08; YEAR=2009
DATADIR=DARTDIAG
make
for LAYER in 1 12 25 ; do
sed -e  's;EXPDEF;'${EXPDEF}';'    -e 's;EXPNUM;'${EXPNUM}';' -e \
	's;INITIALDAY;'${INITIALDAY}';' -e 's;FINALDAY;'${FINALDAY}';' -e \
	's;YEAROFNC;'${YEAR}';' -e 's;TOOL2RUN;'${TOOLCODE}';' -e \
	's;LEVEL2RUN;'${LAYER}';' -e \
	's;RUNLENGTH;365;' -e 's;TIMESTEP;1;' namelist.config.template > namelist.config 

./feom_post_mesh.x
i=${INITIALDAY}; j=${i}; DAYLOOP=${FINALDAY}; EXPERIMENT=( ${EXPDEF}${EXPNUM} ); FILE=INO
for EXP in ${EXPERIMENT[@]}; do
if [[ ${FILE} == 'OCE' ]] && [[ ${LAYER} == 1 ]]; then VAR=(SAL TEM SSH); fi
if [[ ${FILE} == 'OCE' ]] && [[ ${LAYER} != 1 ]]; then VAR=(SAL TEM); fi
if [[ ${FILE} == 'FRC' ]]; then VAR=(RELAX_SALT RUNOFF WNET EVAP); fi
if [[ ${FILE} == 'INO' ]]; then VAR=(SAL TEM); fi
while [ ${i} -le ${DAYLOOP} ];do
		for VARIABLE in ${VAR[@]}; do
			sed -e 's/DAY2PLOT/'${i}'/' -e \
			       's/YEAR2PLOT/'${YEAR}'/' -e \
			       's/EXPCODE/'${EXP}'/' -e \
			       's;^DATADIR=.*$;DATADIR='${DATADIR}';' -e \
			       's/VARIABLE2PLOT/'${VARIABLE}'/' -e \
			       's/LAYERNUMBER/'${LAYER}'/' gmt.TSSHRZ${FILE}_TeMPLaTe.sh > TSSHRZPLT.sh
			sh TSSHRZPLT.sh 
		done
	i=$[ ${i}+1 ]
	done
	done
	i=${j}
done
exit
