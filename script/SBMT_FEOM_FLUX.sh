#!/bin/bash
#BSUB -a poe               #
#BSUB -J FEOM_POST         # Name of the job.
#BSUB -o FEOM_POST_%J.out  # Appends std output to file %J.out.
#BSUB -e FEOM_POST_%J.out  # Appends std error to file %J.err.
#BSUB -P fesom             # Project ID.
#BSUB -q serial_6h         # queue
####BSUB -x                      

TEMPLATE=TeMPLaTe; COPY='cp -f'; REMOVE='rm -f';
LINK='ln -sf'; MOVE='mv -f'

TOOLCODE=27
PROGDIR=/users/home/ans051/FEOM_POSTPROC/MESH_READ
WORKDIR=/work/ans051/TSS/
POSTDIR=${WORKDIR}/POSTPROC
DATADIR=${POSTDIR}/FLX
EXPDEF=BLK
EXPNUM=02
YEAR=EXPYEAR
INITIALDAY=1
FINALDAY=365
YEARS=( 2009 2010 2011 2012 2013 )
LAYERS=( 1 )

cd ${PROGDIR}; make
cd ${DATADIR}; ${LINK} ${PROGDIR}/elem2d.new .

${LINK} ${PROGDIR}/feom_post_mesh.x .

for YR in ${YEARS[@]};do
for SECTION in NB SB ND SD; do
for LAYER in ${LAYERS[@]}; do
	echo "YEAR: :${YR} LAYER: ${LAYER}"
TMPLFILE=${PROGDIR}/namelist.config.template
SBMTFILE=${DATADIR}/namelist.config

sed -e  's;EXPDEF;'${EXPDEF}';'         -e 's;EXPNUM;'${EXPNUM}';'     -e \
	's;INITIALDAY;'${INITIALDAY}';' -e 's;FINALDAY;'${FINALDAY}';' -e \
	's;YEAROFNC;'${YR}';'           -e 's;TOOL2RUN;'${TOOLCODE}';' -e \
	's;LEVEL2RUN;'${LAYER}';'       -e 's;RUNLENGTH;365;'          -e \
        's;flux_section=.*$;flux_section=\x27'${SECTION}'\x27;g'           -e \
	's;TIMESTEP;1;' ${TMPLFILE} > ${SBMTFILE}

./feom_post_mesh.x

done
done
done
