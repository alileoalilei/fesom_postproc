#!/bin/bash
#BSUB -J DART_POST         # Name of the job.
#BSUB -o DART_POST_%J.out  # Appends std output to file %J.out.
#BSUB -e DART_POST_%J.out  # Appends std error to file %J.err.
#BSUB -q serial_30min      # queue
############################################################################
#### SCRIPT TO SPLIT THE STATE VECTOR AND CREATE OUTPUT ####################
##### SUITABLE FOR READ BY FEOM SERIAL ROUTINES ############################
############################################################################

WRKDIR=/work/ans051/TSS
INITIALDAY=1; FINALDAY=1; EXPDEF=EXPCODE; EXPNUM=EXPNO;
EXPYR=EXPERIMENTYEAR
EXPINFO=${EXPDEF}${EXPNUM}
DATADIR=DARTDIAG
FILDIR=${WRKDIR}/${EXPINFO}/FILTER
REMOVE='rm -f'; COPY='cp -f'; MOVE='mv -f'
nodes_3d=3267066
state_kind=2; kind=(salt temp)
i=1; fst_node=0

FILE=Innov.cdf; ${REMOVE} ${FILE}

ncdiff ${FILDIR}/Prior_Diag.nc ${FILDIR}/Posterior_Diag.nc Innovation.cdf 
while [ ${i} -le ${state_kind} ];do
	index=$[ ${i} - 1 ]

fst_node+=($(echo ${fst_node[${index}]}+${nodes_3d} | bc )); 
lst_node+=($(echo ${fst_node[${index}]}+${nodes_3d}-1 | bc )); 

ncks -d StateVariable,${fst_node[${index}]},${lst_node[${index}]} \
                      Innovation.cdf Innovation_${kind[${index}]}.cdf
ncrename -v state,${kind[${index}]} Innovation_${kind[${index}]}.cdf Innov_${kind[${index}]}.cdf

fileindex+=(Innov_${kind[${index}]}.cdf)

ncks -A ${fileindex[${index}]} ${FILE}

${REMOVE} Innovation_${kind[${index}]}.cdf

i=$[ ${i} + 1 ]

done

${REMOVE} ${fileindex[@]} Innovation.cdf 

${COPY} ${FILE} ${FILDIR}

#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#!! INNOVATION STATISTICS      !!!!!!!!!!!!!!!
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
sed -e 's;INIDAY;'${INITIALDAY}';g' -e \
       's;ENDDAY;'${FINALDAY}';g' -e \
       's;EXPNAME;'${EXPDEF}';g' -e \
       's;EXPNUMBER;'${EXPNUM}';g' -e \
       's;^DATADIR=.*$;DATADIR='${DATADIR}';g' -e \
       's;EXPYEAR;'${EXPYR}';g' \
       SBMT_FEOM_SECT_INO_TeMPLaTe.sh > SBMT_FEOM_SECT_INO.sh
bsub < SBMT_FEOM_SECT_INO.sh
date
exit
