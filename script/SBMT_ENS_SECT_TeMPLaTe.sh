#!/bin/bash
#BSUB -a poe               #
#BSUB -J FEOM_POST         # Name of the job.
#BSUB -o FEOM_POST_%J.out  # Appends std output to file %J.out.
#BSUB -e FEOM_POST_%J.out  # Appends std error to file %J.err.
#BSUB -P fesom             # Project ID.
#BSUB -q serial_6h           # queue
####BSUB -x                      
TOOLCODE=7
ENSDEF=ENS; ENSMEM=30
INITIALDAY=INIDAY; FINALDAY=ENDDAY
EXPDEF=EXPNAME; EXPNUM=EXPNO; YEAR=EXPYEAR
make
#for ENSNO in $( seq 1 ${ENSMEM} ); do
#	ENSNUM=$( echo ${ENSNO} | awk '{ printf("%02d\n", $1) }' )
	for LAYER in 1 12 25 60; do
sed -e  's;EXPDEF;'${EXPDEF}';'    -e 's;EXPNUM;'${EXPNUM}';' -e \
        's;ENSDEF;'${ENSDEF}';'    -e 's;ENSNUM;'${ENSMEM}';' -e \
        's;ENSEMBLEMEMBER;'${ENSMEM}';' -e \
	's;INITIALDAY;'${INITIALDAY}';' -e 's;FINALDAY;'${FINALDAY}';' -e \
	's;YEAROFNC;'${YEAR}';' -e 's;TOOL2RUN;'${TOOLCODE}';' -e \
	's;LEVEL2RUN;'${LAYER}';' -e \
	's;RUNLENGTH;365;' -e 's;TIMESTEP;1;' namelist.config.ensemble.template > namelist.config 

./feom_post_mesh.x
i=${INITIALDAY}; j=${i}; DAYLOOP=${FINALDAY}; EXPERIMENT=( ${EXPDEF}${EXPNUM} ); FILE=OCE
for EXP in ${EXPERIMENT[@]}; do
if [[ ${FILE} == 'OCE' ]] && [[ ${LAYER} == 1 ]]; then VAR=(SAL TEM SSH); fi
if [[ ${FILE} == 'OCE' ]] && [[ ${LAYER} != 1 ]]; then VAR=(SAL TEM); fi
if [[ ${FILE} == 'FRC' ]]; then VAR=(RELAX_SALT RUNOFF WNET EVAP); fi
while [ ${i} -le ${DAYLOOP} ];do
		for VARIABLE in ${VAR[@]}; do
			sed -e 's/DAY2PLOT/'${i}'/' -e \
			       's/YEAR2PLOT/'${YEAR}'/' -e \
			       's/EXPCODE/'${EXP}'/' -e \
			       's/VARIABLE2PLOT/'${VARIABLE}'/' -e \
			       's/LAYERNUMBER/'${LAYER}'/' gmt.TSSENS${FILE}_TeMPLaTe.sh > TSSHRZPLT.sh
			sh TSSHRZPLT.sh 
		done
	i=$[ ${i}+1 ]
	done
	done
	i=${j}
	done
#done
exit
