#!/bin/bash
#BSUB -a poe               #
#BSUB -J FEOM_POST         # Name of the job.
#BSUB -o FEOM_POST_%J.out  # Appends std output to file %J.out.
#BSUB -e FEOM_POST_%J.out  # Appends std error to file %J.err.
#BSUB -P fesom             # Project ID.
#BSUB -q serial_6h         # queue
####BSUB -x                      
TOOLCODE=15
PROGDIR=/users/home/ans051/FEOM_POSTPROC/MESH_READ
WORKDIR=/work/ans051/TSS/
POSTDIR=${WORKDIR}/POSTPROC
DATADIR=${POSTDIR}/TKE
YEARS=( 2008 2009 2010 2011 2012 2013)
EXPLISTPRC=( BLK02 ) ; echo ${EXPLISTPRC[@]}
LSTYEAR=$( echo ${#YEARS[@]} - 1 | bc ); echo ${LSTYEAR}
cd ${PROGDIR}; make
cd ${POSTDIR}
ln -sf ${PROGDIR}/feom_post_mesh.x .
TMPLFILE=${PROGDIR}/namelist.config.template
SBMTFILE=${POSTDIR}/namelist.config
for EXP in ${EXPLISTPRC[@]};do
for YEAR in ${YEARS[@]}; do
  INITIALDAY=1; 
    if [ -f ${WORKDIR}/${EXP}/${EXP}.${YEAR}.oce.mean.nc ]; then
      if [ ${YEAR} == 2008 ]; then FINALDAY=366; fi
      if [ ${YEAR} != 2008 ]; then FINALDAY=365; fi
      EXPDEF=$(echo ${EXP} | cut -c1-3 ); EXPNUM=$(echo ${EXP} | cut -c4-5 );
      sed -e  's;EXPDEF;'${EXPDEF}';' -e 's;EXPNUM;'${EXPNUM}';' -e \
              's;INITIALDAY;'${INITIALDAY}';' -e \
              's;FINALDAY;'${FINALDAY}';' -e \
              's;YEAROFNC;'${YEAR}';' -e \
	      's;TOOL2RUN;'${TOOLCODE}';' -e 's;LEVEL2RUN;'${LAYER}';' -e \
              's;RUNLENGTH;365;' -e 's;TIMESTEP;1;' ${TMPLFILE} > ${SBMTFILE}
      ./feom_post_mesh.x
    fi
  done
done
exit
