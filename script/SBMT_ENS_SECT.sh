#!/bin/bash
#BSUB -a poe               #
#BSUB -J FEOM_POST         # Name of the job.
#BSUB -o FEOM_POST_%J.out  # Appends std output to file %J.out.
#BSUB -e FEOM_POST_%J.out  # Appends std error to file %J.err.
#BSUB -P fesom             # Project ID.
#BSUB -q serial_6h           # queue
####BSUB -x                      
TOOLCODE=7
ENSDEF=ENS; ENSMEM=30
INITIALDAY=25; FINALDAY=25
EXPDEF=FB0; EXPNUM=02; YEAR=2009
HOMDIR=/users/home/ans051/FEOM_POSTPROC/MESH_READ
OUTDIR=/work/ans051/TSS/POSTPROC/OSSE
#make
cd ${OUTDIR}
#%ln -sf ${HOMDIR}/feom_post_mesh.x .
#for LAYER in 1 5 12; do
for LAYER in 1 5 12; do
#%sed -e  's;EXPDEF;'${EXPDEF}';'    -e 's;EXPNUM;'${EXPNUM}';' -e \
#%        's;ENSDEF;'${ENSDEF}';'    -e 's;ENSNUM;'${ENSMEM}';' -e \
#%        's;ENSEMBLEMEMBER;'${ENSMEM}';' -e \
#%	's;INITIALDAY;'${INITIALDAY}';' -e 's;FINALDAY;'${FINALDAY}';' -e \
#%	's;YEAROFNC;'${YEAR}';' -e 's;TOOL2RUN;'${TOOLCODE}';' -e \
#%	's;LEVEL2RUN;'${LAYER}';' -e \
#%	's;RUNLENGTH;365;' -e 's;TIMESTEP;1;' ${HOMDIR}/namelist.config.ensemble.template > namelist.config 
#%
#%./feom_post_mesh.x
#i=${INITIALDAY}; j=${i}; DAYLOOP=${FINALDAY}; EXPERIMENT=( ${EXPDEF}${EXPNUM} ); FILE=OCE
i=${INITIALDAY}; j=${i}; DAYLOOP=${FINALDAY}; EXPERIMENT=( FB002 ); FILE=OCE
echo ${EXPERIMENT[@]}
for EXP in ${EXPERIMENT[@]}; do
if [[ ${FILE} == 'OCE' ]] && [[ ${LAYER} == 1 ]]; then VAR=(SSH); fi
if [[ ${FILE} == 'OCE' ]] && [[ ${LAYER} == 1 ]]; then VAR=(SAL TEM SSH); fi
if [[ ${FILE} == 'OCE' ]] && [[ ${LAYER} != 1 ]]; then VAR=(SAL TEM); fi
if [[ ${FILE} == 'FRC' ]]; then VAR=(RELAX_SALT RUNOFF WNET EVAP); fi
while [ ${i} -le ${DAYLOOP} ];do
		for VARIABLE in ${VAR[@]}; do
			sed -e 's/DAY2PLOT/'${i}'/' -e \
			       's/YEAR2PLOT/'${YEAR}'/' -e \
			       's/EXPCODE/'${EXP}'/' -e \
			       's/VARIABLE2PLOT/'${VARIABLE}'/' -e \
			       's/LAYERNUMBER/'${LAYER}'/' ${HOMDIR}/gmt.TSSENSMEAN_TeMPLaTe.sh > TSSHRZPLT.sh
			sh TSSHRZPLT.sh 
		done
	i=$[ ${i}+1 ]
	done
	done
	i=${j}
done
exit
