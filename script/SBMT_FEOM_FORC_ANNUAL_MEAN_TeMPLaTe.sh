#!/bin/bash
#BSUB -a poe               #
#BSUB -J FEOM_POST         # Name of the job.
#BSUB -o FEOM_POST_%J.out  # Appends std output to file %J.out.
#BSUB -e FEOM_POST_%J.out  # Appends std error to file %J.err.
#BSUB -P fesom             # Project ID.
#BSUB -q serial_6h         # queue
####BSUB -x                      
TEMPLATE=TeMPLaTe; COPY='cp -f'; REMOVE='rm -f';
LINK='ln -sf'; MOVE='mv -f'

PROGDIR=/users/home/ans051/FEOM_POSTPROC/MESH_READ
WORKDIR=/work/ans051/TSS/
POSTDIR=${WORKDIR}/POSTPROC
DATADIR=${POSTDIR}/FRC
EXPDEF=EXPNAME 
EXPNUM=EXPNO 	
YEAR=EXPYEAR
INITIALDAY=INIMON 
FINALDAY=ENDMON
YEARS=YEAR
VAR=VARIABLE
LAYERS=( 1 )

cd ${PROGDIR}; make
cd ${DATADIR}; ${LINK} ${PROGDIR}/elem2d.new .

${LINK} ${PROGDIR}/feom_post_mesh.x .

for YR in ${YEARS[@]};do
 for LAYER in ${LAYERS[@]}; do
  for VARIABLE in ${VAR[@]}; do
	
  if [ ${VARIABLE} = "WSTC" ];then TOOLCODE=20; fi
  if [ ${VARIABLE} = "BUOY" ];then TOOLCODE=22; fi
  if [ ${VARIABLE} = "FORC" ];then TOOLCODE=23; fi
  if [ ${VARIABLE} = "WWRK" ];then TOOLCODE=24; fi


TMPLFILE=${PROGDIR}/namelist.config.template
SBMTFILE=${DATADIR}/namelist.config

sed -e 's/EXPDEF/'${EXPDEF}'/' -e 's/EXPNUM/'${EXPNUM}'/' -e \
       's/INITIALDAY/'${INITIALDAY}'/' -e 's/FINALDAY/'${FINALDAY}'/' -e \
       's/YEAROFNC/'${YR}'/' -e 's/TOOL2RUN/'${TOOLCODE}'/' -e \
       's/LEVEL2RUN/'${LAYER}'/' -e 's/RUNLENGTH/365/' -e \
       's/TIMESTEP/1/' ${TMPLFILE} > ${SBMTFILE}


./feom_post_mesh.x


#TMPLFILE=${PROGDIR}/gmt.TSSSUBFRC_ANNUAL_MEAN_TeMPLaTe.sh 
#SBMTFILE=${DATADIR}/gmt.TSSSUBFRC_MEAN.sh
#
#i=1; j=${i}; DAYLOOP=${FINALDAY}; EXPERIMENT=( ${EXPDEF}${EXPNUM} ); FILE=OCE
#
#    for EXP in ${EXPERIMENT[@]}; do
#		sed -e 's/DAY2PLOT/'${i}'/' -e \
#		       's/YEAR2PLOT/'${YR}'/' -e \
#		       's/EXPCODE/'${EXP}'/' -e \
#		       's/VARIABLE2PLOT/'${VARIABLE}'/' -e \
#		       's/LAYERNUMBER/'${LAYER}'/' ${TMPLFILE} > ${SBMTFILE} 
#			sh  ${SBMTFILE}
#	i=$[ ${i}+1 ]
#    done   # experiment loop ends
#
#	i=${j}
  done   # variable loop ends
 done   # layer loop ends
done   # year loop ends
