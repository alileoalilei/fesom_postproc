#!/bin/bash
#BSUB -a poe               #
#BSUB -J FEOM_POST         # Name of the job.
#BSUB -o FEOM_POST_%J.out  # Appends std output to file %J.out.
#BSUB -e FEOM_POST_%J.out  # Appends std error to file %J.err.
#BSUB -P fesom             # Project ID.
#BSUB -q serial_6h         # queue
####BSUB -x                      
TOOLCODE=1
DATADIR=/work/ans051/TSS/
make
YEARS=( 2009 )
EXPLISTPRC=( BAS02  )
EXPLISTPLT=( BAS02 ) ; echo ${EXPLISTPLT[@]}
LSTYEAR=$( echo ${#YEARS[@]} - 1 | bc ); echo ${LSTYEAR}
for YEAR in ${YEARS[@]}; do
  INITIALDAY=1; 
  for EXP in ${EXPLISTPRC[@]};do
    if [ -f ${DATADIR}/${EXP}/${EXP}.${YEAR}.oce.nc ]; then
      if [ ${YEAR} == 2008 ]; then FINALDAY=366; fi
      if [ ${YEAR} != 2008 ]; then FINALDAY=365; fi
      EXPDEF=$(echo ${EXP} | cut -c1-3 ); EXPNUM=$(echo ${EXP} | cut -c4-5 );
      sed -e  's;EXPDEF;'${EXPDEF}';'    -e 's;EXPNUM;'${EXPNUM}';' -e \
              's;INITIALDAY;'${INITIALDAY}';'  -e 's;FINALDAY;'${FINALDAY}';' -e \
              's;YEAROFNC;'${YEAR}';' -e \
	      's;TOOL2RUN;'${TOOLCODE}';' -e 's;LEVEL2RUN;'${LAYER}';' -e \
              's;RUNLENGTH;365;' -e 's;TIMESTEP;1;' namelist.config.template > namelist.config 
      ./feom_post_mesh.x
    fi
  done
done
exit
cd MEANEVOLUTION

sed -e 's;VARIABLENAME;SSH;' -e \
       's;EXPLIST;'${EXPLISTPLT}';' -e \
       's;YR1;'${YEARS[0]}';'  -e \
       's;YR2;'${YEARS[${LSTYEAR}]}';' \
       gmt.MEANEVOLUTION_TeMPLaTe.sh > gmt.MEANEVOLUTION.sh
sh gmt.MEANEVOLUTION.sh
sed -e 's;VARIABLENAME;SSS;' -e \
       's;EXPLIST;'${EXPLISTPLT}';' -e \
       's;YR1;'${YEARS[0]}';'  -e \
       's;YR2;'${YEARS[${LSTYEAR}]}';' \
       gmt.MEANEVOLUTION_TeMPLaTe.sh > gmt.MEANEVOLUTION.sh
sh gmt.MEANEVOLUTION.sh
sed -e 's;VARIABLENAME;SST;' -e \
       's;EXPLIST;'${EXPLISTPLT}';' -e \
       's;YR1;'${YEARS[0]}';'  -e\
       's;YR2;'${YEARS[${LSTYEAR}]}';' \
       gmt.MEANEVOLUTION_TeMPLaTe.sh > gmt.MEANEVOLUTION.sh
sh gmt.MEANEVOLUTION.sh
sed -e 's;VARIABLENAME;SAL;' -e \
       's;EXPLIST;'${EXPLISTPLT}';' -e \
       's;YR1;'${YEARS[0]}';'  -e \
       's;YR2;'${YEARS[${LSTYEAR}]}';' \
       gmt.MEANEVOLUTION_TeMPLaTe.sh > gmt.MEANEVOLUTION.sh
sh gmt.MEANEVOLUTION.sh
sed -e 's;VARIABLENAME;TEM;' -e \
       's;EXPLIST;'${EXPLISTPLT}';' -e \
       's;YR1;'${YEARS[0]}';'  -e\
       's;YR2;'${YEARS[${LSTYEAR}]}';' \
       gmt.MEANEVOLUTION_TeMPLaTe.sh > gmt.MEANEVOLUTION.sh
sh gmt.MEANEVOLUTION.sh
sed -e 's;VARIABLENAME;VEL;' -e \
       's;EXPLIST;'${EXPLISTPLT}';' -e \
       's;YR1;'${YEARS[0]}';'  -e\
       's;YR2;'${YEARS[${LSTYEAR}]}';' \
       gmt.MEANEVOLUTION_TeMPLaTe.sh > gmt.MEANEVOLUTION.sh
sh gmt.MEANEVOLUTION.sh
