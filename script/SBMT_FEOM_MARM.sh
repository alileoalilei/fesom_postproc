#!/bin/bash
#BSUB -a poe               #
#BSUB -J FEOM_POST         # Name of the job.
#BSUB -o FEOM_POST_%J.out  # Appends std output to file %J.out.
#BSUB -e FEOM_POST_%J.out  # Appends std error to file %J.err.
#BSUB -P fesom             # Project ID.
#BSUB -q serial_6h         # queue
####BSUB -x                      
TEMPLATE=TeMPLaTe; COPY='cp -f'; REMOVE='rm -f';
LINK='ln -sf'; MOVE='mv -f'

TOOLCODE=4
PROGDIR=/users/home/ans051/FEOM_POSTPROC/MESH_READ
WORKDIR=/work/ans051/TSS/
POSTDIR=${WORKDIR}/POSTPROC
DATADIR=${POSTDIR}/MARMEVOLUTION
EXPDEF=BLK
EXPNUM=02
EXP=${EXPDEF}${EXPNUM}
INITIALDAY=1
FINALDAY=365
YEARS=( 2008 2009 2010 2011 2012 2013)
LAYER=1

cd ${PROGDIR}; make
cd ${DATADIR}; ${LINK} ${PROGDIR}/elem2d.new .

${LINK} ${PROGDIR}/feom_post_mesh.x .


for YEAR in ${YEARS[@]}; do
  INITIALDAY=1; 
    if [ -f ${WORKDIR}/${EXP}/${EXP}.${YEAR}.oce.mean.nc ]; then
      if [ ${YEAR} == 2008 ]; then FINALDAY=366; fi
      if [ ${YEAR} != 2008 ]; then FINALDAY=365; fi

      TMPLFILE=${PROGDIR}/namelist.config.template
      SBMTFILE=${DATADIR}/namelist.config
      sed -e  's;EXPDEF;'${EXPDEF}';'    -e 's;EXPNUM;'${EXPNUM}';' -e \
              's;INITIALDAY;'${INITIALDAY}';'  -e 's;FINALDAY;'${FINALDAY}';' -e \
              's;YEAROFNC;'${YEAR}';' -e \
	      's;TOOL2RUN;'${TOOLCODE}';' -e 's;LEVEL2RUN;'${LAYER}';' -e \
              's;RUNLENGTH;365;' -e 's;TIMESTEP;1;' ${TMPLFILE} > ${SBMTFILE}
      ./feom_post_mesh.x
    fi
done
#cd MARMEVOLUTION
#
#sed -e 's;VARIABLENAME;SSH;' -e \
#       's;EXPLIST;'${EXPLISTPLT}';' -e \
#       's;YR1;'${YEARS[0]}';'  -e \
#       's;YR2;'${YEARS[${LSTYEAR}]}';' \
#       gmt.MARMEVOLUTION_TeMPLaTe.sh > gmt.MARMEVOLUTION.sh
#sh gmt.MARMEVOLUTION.sh
#sed -e 's;VARIABLENAME;SSS;' -e \
#       's;EXPLIST;'${EXPLISTPLT}';' -e \
#       's;YR1;'${YEARS[0]}';'  -e \
#       's;YR2;'${YEARS[${LSTYEAR}]}';' \
#       gmt.MARMEVOLUTION_TeMPLaTe.sh > gmt.MARMEVOLUTION.sh
#sh gmt.MARMEVOLUTION.sh
#sed -e 's;VARIABLENAME;SST;' -e \
#       's;EXPLIST;'${EXPLISTPLT}';' -e \
#       's;YR1;'${YEARS[0]}';'  -e\
#       's;YR2;'${YEARS[${LSTYEAR}]}';' \
#       gmt.MARMEVOLUTION_TeMPLaTe.sh > gmt.MARMEVOLUTION.sh
#sh gmt.MARMEVOLUTION.sh
#sed -e 's;VARIABLENAME;SAL;' -e \
#       's;EXPLIST;'${EXPLISTPLT}';' -e \
#       's;YR1;'${YEARS[0]}';'  -e \
#       's;YR2;'${YEARS[${LSTYEAR}]}';' \
#       gmt.MARMEVOLUTION_TeMPLaTe.sh > gmt.MARMEVOLUTION.sh
#sh gmt.MARMEVOLUTION.sh
#sed -e 's;VARIABLENAME;TEM;' -e \
#       's;EXPLIST;'${EXPLISTPLT}';' -e \
#       's;YR1;'${YEARS[0]}';'  -e\
#       's;YR2;'${YEARS[${LSTYEAR}]}';' \
#       gmt.MARMEVOLUTION_TeMPLaTe.sh > gmt.MARMEVOLUTION.sh
#sh gmt.MARMEVOLUTION.sh
