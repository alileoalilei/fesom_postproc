#!/bin/bash
#----------------------------------------------------------!
#   SUBMIT CREATE OBSERVATIONS                             !
#   THIS SCRIPT USES FEOM_POSTPROC ROUTINES                !
#   IN $RUNDIR TO CREATE SYNTHETIC OBSERVATIONS            !
#   SBMT_CREATE_OBS_TeMPLaTe.sh                            !
#----------------------------------------------------------!
#----------------------------------------------------------!
# Tool code of creating ferrybox observations is 8         !
# Tool code of creating vertical profile obs is 11         !
#----------------------------------------------------------!
function jobid {
  output=$($*)
  echo $output | head -n1 | cut -d'<' -f2 | cut -d'>' -f1
    }
#-------------------------------------------------
HOMDIR=/users/home/ans051
WRKDIR=${HOMDIR}/FEOM_PREPROC/OBSERVATION/OSSE
RUNDIR=${HOMDIR}/FEOM_POSTPROC/MESH_READ
#-------------------------------------------------
    TMPLFILE=${RUNDIR}/SBMT_CREATE_PROFILE_TeMPLaTe.sh
    SBMTFILE=${WRKDIR}/SBMT_CREATE_PROFILE.sh
INITIALDAY=1; FINALDAY=2; EXPDEF=NR0; EXPNUM=01; YEAR=2009
sed -e 's;INIDAY;'${INITIALDAY}';g' -e \
       's;ENDDAY;'${FINALDAY}';g' -e \
       's;EXPNAME;'${EXPDEF}';g' -e \
       's;EXPNO;'${EXPNUM}';g' -e \
       's;EXPYEAR;'${YEAR}';g' \
       ${TMPLFILE} > ${SBMTFILE}
cd ${WRKDIR}; ID=$( jobid bsub < ${SBMTFILE} ); echo ${ID} 
exit
#-------------------------------------------------
    TMPLFILE=${RUNDIR}/SBMT_CREATE_OBS_TeMPLaTe.sh
    SBMTFILE=${WRKDIR}/SBMT_CREATE_OBS.sh
INITIALDAY=1; FINALDAY=2; EXPDEF=NR0; EXPNUM=01; YEAR=2009
sed -e 's;INIDAY;'${INITIALDAY}';g' -e \
       's;ENDDAY;'${FINALDAY}';g' -e \
       's;EXPNAME;'${EXPDEF}';g' -e \
       's;EXPNO;'${EXPNUM}';g' -e \
       's;EXPYEAR;'${YEAR}';g' \
       ${TMPLFILE} > ${SBMTFILE}
cd ${WRKDIR}; ID=$( jobid bsub < ${SBMTFILE} ); echo ${ID} 
#-------------------------------------------------
#-- CONVERT ASCII DATA INTO DART FORMAT ----------
#-------------------------------------------------
#    SBMTFILE=FeoM_SBMT_oBS_TeXT.lsf
#cd ${WRKDIR}; ID=$( jobid bsub -w "done(${ID})" < ${SBMTFILE} ); echo ${ID}
