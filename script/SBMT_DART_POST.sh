#!/bin/bash
#BSUB -a poe               #
#BSUB -J DART_POST         # Name of the job.
#BSUB -o DART_POST_%J.out  # Appends std output to file %J.out.
#BSUB -e DART_POST_%J.out  # Appends std error to file %J.err.
#BSUB -q serial_6h           # queue


debug=1
REMOVE='rm -f'; COPY='cp -f'; MOVE='mv -f'; LINK='ln -sf'
#----------------------------------------------------------

TOOLCODE=9
INITIALDAY=1; FINALDAY=7; EXPNAM=FB0; EXPNUM=02; EXPYR=2009
EXPINFO=${EXPNAM}${EXPNUM}

USRNAM=ans051
USRHOM=/users/home/${USRNAM}
WORKDIR=/work/${USRNAM}/TSS
DRTDIR=${USRHOM}/DART/FEOM/models/FeoM/work
PROGDIR=${USRHOM}/FEOM_POSTPROC/MESH_READ
DARTDIR==${WORKDIR}/POSTPROC
POSTDIR=${WORKDIR}/POSTPROC/DARTDIAG/${EXPINFO}
FLTRDIR=${WORKDIR}/${EXPINFO}/FILTER
NRDIR=${WORKDIR}/NR001

state_kind=2; kind=( salt temp ); gmt_kind=( SAL TEM )
dart_kind=( Salinity Temperature )
diag_kind=( GUESS ANALY )
region=( Marmara Bosphorus )






layer=( 5 )
#----------------------------------------------------------
#--- Diff Nature Run and Prior Diag files -----------------
#----------------------------------------------------------
${REMOVE} ${POSTDIR}/feom_post_mesh.x
for DIAGFILE in $( ls ${FLTRDIR}/Prior_Diag_1490??_{00000,21600,43200,64800}.nc );do 
cd ${POSTDIR}
${LINK} ${PROGDIR}/feom_post_mesh.x .

DARTIME=( $( echo ${DIAGFILE} | sed 's;.*[/Prior];;g'   | sed 's;_\|:\|\.\|,; ;g' | sed 's/[A-Za-z]*//g' ) )
DARTDAY=$( echo ${DARTIME[0]} | awk '{ printf("%06d\n",$1) }' )
DARTSEC=$( echo ${DARTIME[1]} | awk '{ printf("%05d\n",$1) }' )
FEOMDAY=$( echo "${DARTDAY} - 149019" | bc )
FEOMSEC=$( echo "scale=2; ${DARTSEC} / 86400 * 24" | bc )
NRSTEP=$( echo "scale=0; ${FEOMDAY}*24 + ${FEOMSEC} / 1" | bc )

     PRIOR=${FLTRDIR}/Prior_Diag_${DARTDAY}_${DARTSEC}.nc
 NR=${NRDIR}/NR001.2009.oce.nc
INNOVATION=${FLTRDIR}/Innovation_Diag_${DARTDAY}_${DARTSEC}.cdf
state_vars=$( echo ${kind[@]} | sed 's; ;,;g' )

#if [[ ${debug} > 0 ]]; then
#  ls ${PRIOR} 
#  ls ${NR} 
#  echo ${INNOVATION}
#  echo ${state_vars}
#  echo ${NRSTEP} ${FEOMSEC} ${FEOMDAY} ${DARTSEC} ${DARTDAY} ${DARTIME[@]}
#fi
##
#ncks -O -v ${state_vars} -d T,${NRSTEP} ${NR} ${FLTRDIR}/naturerun.cdf
##ncwa -O -a T ${FLTRDIR}/naturerun_copy.cdf ${FLTRDIR}/naturerun.cdf
#ncrename -d T,time ${FLTRDIR}/naturerun.cdf
#ncks -O -v ${state_vars} -d copy,0 ${PRIOR} ${FLTRDIR}/prior_copy.cdf
#ncwa -O -d copy,0 -a copy ${FLTRDIR}/prior_copy.cdf ${FLTRDIR}/prior.cdf
#ncdiff -O -v ${state_vars} ${FLTRDIR}/prior.cdf ${FLTRDIR}/naturerun.cdf ${INNOVATION}
##cp ${FLTRDIR}/naturerun_copy.cdf ${INNOVATION}
##
#${REMOVE} ${FLTRDIR}/prior.cdf ${FLTRDIR}/naturerun.cdf 
#${REMOVE} ${FLTRDIR}/prior_copy.cdf #${FLTRDIR}/naturerun_copy.cdf
#${REMOVE} ${FLTRDIR}/Innovation.cdf
#${COPY} ${INNOVATION} ${FLTRDIR}/Innovation.cdf
##
#level=$( echo ${layer[@]} | sed 's/\ /,/g' )
##
#if [[ ${debug} > 0 ]]; then echo ${level}; fi
#TMPLFILE=${PROGDIR}/namelist.config.template
#SBMTFILE=${POSTDIR}/namelist.config
##
#sed -e "s;EXPDEF;${EXPNAM};g" -e "s;EXPNUM;${EXPNUM};g" -e "s;^dart_days=.*$;dart_days=${DARTDAY};g" -e "s;^dart_secs=.*$;dart_secs=${DARTSEC};g" -e "s;^iniday=.*$;iniday=${INITIALDAY};g" -e "s;^endday=.*$;endday=${FINALDAY};g" -e "s;^runyear=.*$;runyear=${EXPYR};g" -e "s;^tool=.*$;tool=${TOOLCODE};g" -e "s;^levelname=.*$;levelname=${level};g" -e "s;^step_per_day=.*$;step_per_day=1;g" -e "s;^run_length=.*$;run_length=1;g" ${TMPLFILE} > ${SBMTFILE} 
##
#./feom_post_mesh.x
#	
cd ${POSTDIR}; ${LINK} ${PROGDIR}/elem2d.new .
if [[ ${debug} > 0 ]]; then echo ${POSTDIR}; fi
TMPLFILE=${PROGDIR}/gmt.TSSHRZINO_TeMPLaTe.sh
SBMTFILE=${POSTDIR}/gmt.TSSHRZINO.sh
for LAYER in ${layer[@]} ; do
  for VARIABLE in ${gmt_kind[@]}; do
  sed -e "s;^FEOMDAY=.*$;FEOMDAY=${FEOMDAY};g" -e "s;^YEAR=.*$;YEAR=${EXPYR};g" -e "s;^EXP=.*$;EXP=${EXPINFO};g" -e "s;^DATADIR=.*$;DATADIR=${POSTDIR};g" -e "s;^LAYERNUMBER=.*$;LAYERNUMBER=${LAYER};g" -e "s;^VAR=.*$;VAR=${VARIABLE};g" -e "s;^DARTDAY=.*$;DARTDAY=${DARTDAY};g" -e "s;^DARTSEC=.*$;DARTSEC=${DARTSEC};g" ${TMPLFILE} > ${SBMTFILE}
  sh ${SBMTFILE}
  done
done
done


exit
#---------------------------------------------------------
#---- Observation Diagnostics ----------------------------
#---------------------------------------------------------
#> here first call obs_seq_to_netcdf 
cd ${FLTRDIR}; ls -1 obs_seq.final.1490??_????? > obsFile_list
${REMOVE} obs_epoch_???.nc; 
${LINK} ${DRTDIR}/obs_seq_to_netcdf . ;  ./obs_seq_to_netcdf 
${LINK} ${DRTDIR}/obs_diag . ;  ./obs_diag 
cd ${POSTDIR}; 
for VAR in ${gmt_kind[@]}; do
	for TIME in $( seq ${INITIALDAY} ${FINALDAY} ); do 
	 TIM=$( echo ${TIME} | awk '{ printf("%03d\n",$1) }' )
	 sed -e 's;VARIABLENAME;'${VAR}';g' -e \
	        's;BINCURRENTTIME;'${TIM}';g' -e \
		's;EXPERIMENTNAME;'${EXPINFO}';g' \
		${PROGDIR}/frt.obs_epoch_TeMPLaTe.jnl > frt.obs_epoch.jnl
	 pyferret -nodisplay -script frt.obs_epoch.jnl 
	done 
	for KIND in PROF FBOX ; do 
	 for BorA in GUESS ANALY ; do 
	  for REGID in $( seq 1 2 ); do 
	   sed -e 's;VARIABLENAME;'${VAR}';g' -e \
	          's;VARIABLEKIND;'${KIND}';g' -e \
	          's;PARAMETER;'${BorA}';g' -e \
	          's;REGIONID;'${REGID}';g' -e \
		  's;EXPERIMENTNAME;'${EXPINFO}';g' \
		  ${PROGDIR}/frt.OBS_DIAG_TeMPLaTe.jnl > frt.obs_diag.jnl
	   pyferret -nodisplay -script frt.obs_diag.jnl 
          done
         done
	done 

done
exit
layer=( 1 5 20 )
#----------------------------------------------------------
#--- Diff Posterior and Prior Diag files ------------------
#----------------------------------------------------------
${REMOVE} ${POSTDIR}/feom_post_mesh.x
for DIAGFILE in $( ls ${FLTRDIR}/Prior_Diag_1490??_?????.nc );do 
cd ${POSTDIR}
${LINK} ${PROGDIR}/feom_post_mesh.x .

DARTIME=( $( echo ${DIAGFILE} | sed 's;.*[/Prior];;g'   | \
     sed 's;_\|:\|\.\|,; ;g' | sed 's/[A-Za-z]*//g' ) )
DARTDAY=$( echo ${DARTIME[0]} | awk '{ printf("%06d\n",$1) }' )
DARTSEC=$( echo ${DARTIME[1]} | awk '{ printf("%05d\n",$1) }' )
FEOMDAY=$( echo "${DARTDAY} - 149018" | bc )
FEOMSEC=$( echo "scale=2;${DARTSEC} / 86400" | bc )

     PRIOR=${FLTRDIR}/Prior_Diag_${DARTDAY}_${DARTSEC}.nc
 POSTERIOR=${FLTRDIR}/Posterior_Diag_${DARTDAY}_${DARTSEC}.nc
INCREMENT=${FLTRDIR}/Increment_Diag_${DARTDAY}_${DARTSEC}.cdf
state_vars=$( echo ${kind[@]} | sed 's; ;,;g' )

if [[ ${debug} > 0 ]]; then
  echo ${PRIOR} 
  echo ${POSTERIOR} 
  echo ${INCREMENT}
  echo ${state_vars}
fi
ncdiff -O -v ${state_vars} ${PRIOR} ${POSTERIOR} ${INCREMENT}

${REMOVE} ${FLTRDIR}/Increment.cdf
${COPY} ${INCREMENT} ${FLTRDIR}/Increment.cdf

level=$( echo ${layer[@]} | sed 's/\ /,/g' )

if [[ ${debug} > 0 ]]; then echo ${level}; fi
TMPLFILE=${PROGDIR}/namelist.config.template
SBMTFILE=${POSTDIR}/namelist.config

sed -e "s;EXPDEF;${EXPNAM};g" -e "s;EXPNUM;${EXPNUM};g" -e "s;^dart_days=.*$;dart_days=${DARTDAY};g" -e "s;^dart_secs=.*$;dart_secs=${DARTSEC};g" -e "s;^iniday=.*$;iniday=${INITIALDAY};g" -e "s;^endday=.*$;endday=${FINALDAY};g" -e "s;^runyear=.*$;runyear=${EXPYR};g" -e "s;^tool=.*$;tool=${TOOLCODE};g" -e "s;^levelname=.*$;levelname=${level};g" -e "s;^step_per_day=.*$;step_per_day=1;g" -e "s;^run_length=.*$;run_length=1;g" ${TMPLFILE} > ${SBMTFILE} 

./feom_post_mesh.x
	
cd ${POSTDIR}; ${LINK} ${PROGDIR}/elem2d.new .
if [[ ${debug} > 0 ]]; then echo ${POSTDIR}; fi
TMPLFILE=${PROGDIR}/gmt.TSSHRZINC_TeMPLaTe.sh
SBMTFILE=${POSTDIR}/gmt.TSSHRZINC.sh
for LAYER in ${layer[@]} ; do
  for VARIABLE in ${gmt_kind[@]}; do
  sed -e "s;^FEOMDAY=.*$;FEOMDAY=${FEOMDAY};g" -e "s;^YEAR=.*$;YEAR=${EXPYR};g" -e "s;^EXP=.*$;EXP=${EXPINFO};g" -e "s;^DATADIR=.*$;DATADIR=${POSTDIR};g" -e "s;^LAYERNUMBER=.*$;LAYERNUMBER=${LAYER};g" -e "s;^VAR=.*$;VAR=${VARIABLE};g" -e "s;^DARTDAY=.*$;DARTDAY=${DARTDAY};g" -e "s;^DARTSEC=.*$;DARTSEC=${DARTSEC};g" ${TMPLFILE} > ${SBMTFILE}
  sh ${SBMTFILE}
  done
done
done






