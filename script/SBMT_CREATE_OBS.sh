#!/bin/bash
#BSUB -a poe               #
#BSUB -J FEOM_POST         # Name of the job.
#BSUB -o FEOM_POST_%J.out  # Appends std output to file %J.out.
#BSUB -e FEOM_POST_%J.out  # Appends std error to file %J.err.
#BSUB -P fesom             # Project ID.
#BSUB -q serial_30min      # queue
####BSUB -x                      
TOOLCODE=8
INITIALDAY=1; FINALDAY=1
EXPDEF=WFX; EXPNUM=04; YEAR=2009
make
sed -e  's;EXPDEF;'${EXPDEF}';'    -e 's;EXPNUM;'${EXPNUM}';' -e \
	's;INITIALDAY;'${INITIALDAY}';'  -e 's;FINALDAY;'${FINALDAY}';' -e \
	's;YEAROFNC;'${YEAR}';' -e 's;TOOL2RUN;'${TOOLCODE}';' -e \
	's;LEVEL2RUN;'${LAYER}';' -e \
	's;RUNLENGTH;365;' -e 's;TIMESTEP;1;' namelist.config.template > namelist.config 

./feom_post_mesh.x
exit

