#!/bin/bash
#BSUB -a poe               #
#BSUB -J FEOM_POST         # Name of the job.
#BSUB -o FEOM_POST_%J.out  # Appends std output to file %J.out.
#BSUB -e FEOM_POST_%J.out  # Appends std error to file %J.err.
#BSUB -P fesom             # Project ID.
#BSUB -q serial_6h         # queue
####BSUB -x                      

TEMPLATE=TeMPLaTe; COPY='cp -f'; REMOVE='rm -f';
LINK='ln -sf'; MOVE='mv -f'

TOOLCODE=21
PROGDIR=/users/home/ans051/FEOM_POSTPROC/MESH_READ
WORKDIR=/work/ans051/TSS/
POSTDIR=${WORKDIR}/POSTPROC
DATADIR=${POSTDIR}/FLX
EXPDEF=EXPNAME; 
EXPNUM=EXPNO; 
YEAR=EXPYEAR
INITIALDAY=1 
FINALDAY=1
YEARS=( 2008 )
LAYERS=( 1 )

cd ${PROGDIR}; make
cd ${DATADIR}; ${LINK} ${PROGDIR}/elem2d.new .

${LINK} ${PROGDIR}/feom_post_mesh.x .

for YR in ${YEARS[@]};do
for LAYER in ${LAYERS[@]}; do
for SECTION in NB MB SB ND SD; do
	echo "YEAR: :${YR} LAYER: ${LAYER}"
TMPLFILE=${PROGDIR}/namelist.config.template
SBMTFILE=${DATADIR}/namelist.config

sed -e  's;EXPDEF;'${EXPDEF}';'         -e 's;EXPNUM;'${EXPNUM}';'     -e \
	's;INITIALDAY;'${INITIALDAY}';' -e 's;FINALDAY;'${FINALDAY}';' -e \
	's;YEAROFNC;'${YR}';'           -e 's;TOOL2RUN;'${TOOLCODE}';' -e \
	's;LEVEL2RUN;'${LAYER}';'       -e 's;RUNLENGTH;365;'          -e \
        's;flux_section=.*$;flux_section=\x27'${SECTION}'\x27;g'           -e \
	's;TIMESTEP;1;' ${TMPLFILE} > ${SBMTFILE}

./feom_post_mesh.x

#i=${INITIALDAY}
#while [ ${i} -le ${FINALDAY} ]; do
#	DAY=$(echo ${i} | awk '{printf("%03d\n",$1)}')
#	for VARIABLE in TEM SAL;do
#	sed -e  's;VARIABLENAME;'${VARIABLE}';' -e \
#		's;EXPDEF;'${EXPDEF}';' -e 's;EXPNUM;'${EXPNUM}';' -e \
#		's;YEAROFNC;'${YEAR}';' -e 's;DAYOFNC;'${DAY}';' \
#		gmt.THALWEG_TeMPLaTe.sh > gmt.THALWEG.sh
#	sh gmt.THALWEG.sh
#	done
#	i=$[ ${i} + 1 ]
done
done
done
