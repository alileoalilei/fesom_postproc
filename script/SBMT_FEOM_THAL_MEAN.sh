#!/bin/bash
#BSUB -a poe               #
#BSUB -J FEOM_POST         # Name of the job.
#BSUB -o FEOM_POST_%J.out  # Appends std output to file %J.out.
#BSUB -e FEOM_POST_%J.out  # Appends std error to file %J.err.
#BSUB -P fesom             # Project ID.
#BSUB -q serial_6h         # queue
####BSUB -x                      
TOOLCODE=18
PROGDIR=/users/home/ans051/FEOM_POSTPROC/MESH_READ
WORKDIR=/work/ans051/TSS/
POSTDIR=${WORKDIR}/POSTPROC
DATADIR=${POSTDIR}/CALCTMEAN
INITIALDAY=1; FINALDAY=1
EXPDEF=BLK; EXPNUM=01; YEAR=2008
YEARS=( 2008 2009 2010 2011 2012 2013 )
cd ${PROGDIR}; make
cd ${DATADIR}
ln -sf ${PROGDIR}/feom_post_mesh.x .
for YR in ${YEARS[@]}; do
TMPLFILE=${PROGDIR}/namelist.config.template
SBMTFILE=${DATADIR}/namelist.config
	echo "YEAR: :${YR}"
sed -e  's;EXPDEF;'${EXPDEF}';'    -e 's;EXPNUM;'${EXPNUM}';' -e \
	's;INITIALDAY;'${INITIALDAY}';'  -e 's;FINALDAY;'${FINALDAY}';' -e \
	's;YEAROFNC;'${YR}';' -e 's;TOOL2RUN;'${TOOLCODE}';' -e \
	 's;TOOL2RUN;'${TOOLCODE}';' -e 's;LEVEL2RUN;'${LAYER}';' -e \
	's;RUNLENGTH;365;' -e 's;TIMESTEP;1;' ${TMPLFILE} > ${SBMTFILE}
./feom_post_mesh.x
TMPLFILE=${PROGDIR}/gmt.THALWEG_ANNUAL_MEAN_TeMPLaTe.sh
SBMTFILE=${DATADIR}/gmt.THALWEG_MEAN.sh
	DAY=$(echo ${i} | awk '{printf("%02d\n",$1)}')
	for VARIABLE in TEM SAL DEN; do
	sed -e  's;VARIABLENAME;'${VARIABLE}';' -e \
		's;EXPDEF;'${EXPDEF}';' -e 's;EXPNUM;'${EXPNUM}';' -e \
		's;YEAROFNC;'${YR}';' -e 's;DAYOFNC;'${DAY}';' \
                 ${TMPLFILE} > ${SBMTFILE}
	sh ${SBMTFILE}
	done
done
exit
