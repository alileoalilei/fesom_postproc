#!/bin/bash
#BSUB -a poe               #
#BSUB -J FEOM_POST         # Name of the job.
#BSUB -o FEOM_POST_%J.out  # Appends std output to file %J.out.
#BSUB -e FEOM_POST_%J.out  # Appends std error to file %J.err.
#BSUB -P fesom             # Project ID.
#BSUB -q serial_6h         # queue
####BSUB -x                      
TEMPLATE=TeMPLaTe; COPY='cp -f'; REMOVE='rm -f';
LINK='ln -sf'; MOVE='mv -f'

TOOLCODE=19
PROGDIR=/users/home/ans051/FEOM_POSTPROC/MESH_READ
WORKDIR=/work/ans051/TSS/
POSTDIR=${WORKDIR}/POSTPROC
DATADIR=${POSTDIR}/VORTSMEAN
EXPDEF=EXPNAME 
EXPNUM=EXPNO
YEAR=EXPYEAR
INITIALDAY=INIMON
FINALDAY=ENDMON
YEARS=YEAR
LAYERS=( 1 )

cd ${PROGDIR}; make
cd ${DATADIR}; ${LINK} ${PROGDIR}/elem2d.new .

${LINK} ${PROGDIR}/feom_post_mesh.x .

for YR in ${YEARS[@]};do
for LAYER in ${LAYERS[@]}; do
	echo "YEAR: :${YR} LAYER: ${LAYER}"
TMPLFILE=${PROGDIR}/namelist.config.template
SBMTFILE=${DATADIR}/namelist.config

sed -e 's/EXPDEF/'${EXPDEF}'/' -e 's/EXPNUM/'${EXPNUM}'/' -e \
       's/INITIALDAY/'${INITIALDAY}'/' -e 's/FINALDAY/'${FINALDAY}'/' -e \
       's/YEAROFNC/'${YR}'/' -e 's/TOOL2RUN/'${TOOLCODE}'/' -e \
       's/LEVEL2RUN/'${LAYER}'/' -e 's/RUNLENGTH/365/' -e \
       's/TIMESTEP/1/' ${TMPLFILE} > ${SBMTFILE}


./feom_post_mesh.x


TMPLFILE=${PROGDIR}/gmt.VORTICITY_MEAN_TeMPLaTe.sh 
SBMTFILE=${DATADIR}/gmt.VORTICITY_MEAN.sh
i=1; j=${i}; DAYLOOP=${FINALDAY}; EXPERIMENT=( ${EXPDEF}${EXPNUM} )

for EXP in ${EXPERIMENT[@]}; do
		sed -e 's/DAY2PLOT/'${i}'/' -e \
		       's/YEAR2PLOT/'${YR}'/' -e \
		       's/EXPCODE/'${EXP}'/' -e \
		       's/LAYERNUMBER/'${LAYER}'/' ${TMPLFILE} > ${SBMTFILE} 
			sh  ${SBMTFILE}
done
done 
done
