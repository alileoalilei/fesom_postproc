#!/bin/bash
#BSUB -J FEOM_POST         # Name of the job.
#BSUB -o LOG/FEOM_POST_%J.out  # Appends std output to file %J.out.
#BSUB -e LOG/FEOM_POST_%J.out  # Appends std error to file %J.err.
#BSUB -q serial_6h         # queue

EXE=feom_post_mesh.x
RUNDIR=/users/home/ans051/FEOM_POSTPROC/MESH_READ
TOOLCODE=11
INITIALDAY=INIDAY; FINALDAY=ENDDAY
EXPDEF=EXPNAME; EXPNUM=EXPNO; YEAR=EXPYEAR
TEMPLATE=TeMPLaTe; COPY='cp -f'; REMOVE='rm -f'; 
LINK='ln -sf'; MOVE='mv -f'
sed -e  's;EXPDEF;'${EXPDEF}';'    -e 's;EXPNUM;'${EXPNUM}';' -e \
	's;INITIALDAY;'${INITIALDAY}';'  -e 's;FINALDAY;'${FINALDAY}';' -e \
	's;YEAROFNC;'${YEAR}';' -e 's;TOOL2RUN;'${TOOLCODE}';' -e \
	's;LEVEL2RUN;'${LAYER}';' -e \
	's;RUNLENGTH;365;' -e 's;TIMESTEP;1;'\
       	${RUNDIR}/namelist.config.template > namelist.config 

${LINK} ${RUNDIR}/${EXE} .

./${EXE}
