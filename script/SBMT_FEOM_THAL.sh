#!/bin/bash
#BSUB -a poe               #
#BSUB -J FEOM_POST         # Name of the job.
#BSUB -o FEOM_POST_%J.out  # Appends std output to file %J.out.
#BSUB -e FEOM_POST_%J.out  # Appends std error to file %J.err.
#BSUB -P fesom             # Project ID.
#BSUB -q serial_30min      # queue
####BSUB -x                      
TOOLCODE=2
INITIALDAY=1; FINALDAY=1
EXPDEF=LEP; EXPNUM=PC; YEAR=2008
make
sed -e  's;EXPDEF;'${EXPDEF}';'    -e 's;EXPNUM;'${EXPNUM}';' -e \
	's;INITIALDAY;'${INITIALDAY}';'  -e 's;FINALDAY;'${FINALDAY}';' -e \
	's;YEAROFNC;'${YEAR}';' -e 's;TOOL2RUN;'${TOOLCODE}';' -e \
	 's;TOOL2RUN;'${TOOLCODE}';' -e 's;LEVEL2RUN;'${LAYER}';' -e \
	's;RUNLENGTH;365;' -e 's;TIMESTEP;1;' namelist.config.template > namelist.config 

./feom_post_mesh.x
i=${INITIALDAY}
while [ ${i} -le ${FINALDAY} ]; do
	DAY=$(echo ${i} | awk '{printf("%03d\n",$1)}')
	for VARIABLE in TEM SAL;do
	sed -e  's;VARIABLENAME;'${VARIABLE}';' -e \
		's;EXPDEF;'${EXPDEF}';' -e 's;EXPNUM;'${EXPNUM}';' -e \
		's;YEAROFNC;'${YEAR}';' -e 's;DAYOFNC;'${DAY}';' \
		gmt.THALWEG_TeMPLaTe.sh > gmt.THALWEG.sh
	sh gmt.THALWEG.sh
	done
	i=$[ ${i} + 1 ]
done
exit

